FROM mcr.microsoft.com/dotnet/sdk:7.0-alpine3.16 AS builder

ENV PROTOBUF_PROTOC=/usr/bin/protoc
ENV gRPC_PluginFullPath=/usr/bin/grpc_csharp_plugin

RUN apk add --no-cache git protobuf protobuf-dev grpc

COPY . /ACSharp
WORKDIR /ACSharp

RUN mkdir -p /app
RUN dotnet publish ACSharp.sln --configuration Release \
           -p:GitHash=$(git rev-parse HEAD) \
           -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) \
           -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) \
           -p:GitTag=$(git describe --tags | sed 's/-g.*//') \
           -p:PublishDir=/app


FROM mcr.microsoft.com/dotnet/aspnet:7.0-jammy-arm32v7 AS run

COPY --from=builder /app /app

# Set the timezone.
ENV TZ=UTC
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime

RUN apt update \
	&& apt -y install joe nano less wget curl bash libgpiod-dev

RUN apt clean
RUN rm -rf /var/lib/apt/lists/*
    
ENV DIR_DATA = "/data"
#ENV AcSharp_WWW = "/www"
ENV HOME = /data
 
WORKDIR /app
ENTRYPOINT ["/usr/bin/dotnet","ACSharp.dll"]
