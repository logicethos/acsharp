using System.Device.Spi;

namespace ACSharpDriver_RaspberryPi;

public class M90E32AS_SPI
{

    SpiDevice spiDevice;

    void Connect()
    {
        var settings = new SpiConnectionSettings(0, 0)
        {
                ClockFrequency = 5000000,
                //ClockFrequency = 500000, // Frequency didn't seem to make any difference
                Mode = SpiMode.Mode0,   // From SemTech docs pg 80 CPOL=0, CPHA=0
        };

        spiDevice = SpiDevice.Create(settings);
    }




}