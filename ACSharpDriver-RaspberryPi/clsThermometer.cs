using Iot.Device.OneWire;

namespace ACSharpDriver_RaspberryPi;


public class clsThermometer
{

  //  public Gauge PrometheusGuage { get; private set; }
    public OneWireThermometerDevice Thermometer { get; private init; }
    public double Celsius { get; set; }
    public string Name { get; init; }

    public clsThermometer(OneWireThermometerDevice thermometer)
    {

        Thermometer = thermometer;


        switch (thermometer.DeviceId)
        {
            case "28-8000001e9efc":
                Name = "H0";
                break;
            case "28-8000001e9f22":
                Name = "H1";
                break;
            case "28-8000001eaff3":
                Name = "Top";
                break;
            case "28-8000001e9e5f":
                Name = "SSR";
                break;
            default:
                Name = "unknown";
                break;
        }


       // PrometheusGuage = Metrics.CreateGauge($"T{thermometer.DeviceId.Replace('-','_')}" , "Celsius");
        //PrometheusGuage.WithLabels(Name);


    }

}