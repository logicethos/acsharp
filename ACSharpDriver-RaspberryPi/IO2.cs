using System.Device.Gpio;
using System.Device.Pwm;
using System.Diagnostics;
using System.Runtime.InteropServices.ComTypes;
using ConsoleServer;
using Iot.Device.OneWire;
using Serilog;
using Spectre.Console;

namespace ACSharpDriver_RaspberryPi;

public class IO2 : IDisposable
{
    private GpioController controller;
//    private PwmChannel PWM0;
//    private PwmChannel PWM1;

    public clsThermometer TopTemp { get; set;}
    public clsThermometer H0Temp { get; set;}
    public clsThermometer H1Temp { get; set;}

    public int PWM0Frequency => 101;
    public int PWM1Frequency => 101;
    public double PWM0DutyCycle { get; set;}
    public double PWM1DutyCycle { get; set;}

    public int MsOn0 { get; set;}
    public int MsOff0 { get; set;}
    public int MsOn1 { get; set;}
    public int MsOff1 { get; set;}

    private uint LoadWatts = 3000;

    //public List<OneWireThermometerDevice> Thermometer = new List<OneWireThermometerDevice>();
    public List<clsThermometer> Thermometers = new List<clsThermometer>();

   // private Gauge PrometheusGuageWatts0;
    //private Gauge PrometheusGuageWatts1;

    public IO2()
    {
        try
        {
#if !DEBUG
            controller = new GpioController();

            controller.OpenPin(12,PinMode.Output, PinValue.Low);
            controller.OpenPin(13,PinMode.Output, PinValue.Low);
#endif
            Thread ssrControlThread0 = new Thread(() => SSRControl0(12));
            ssrControlThread0.Priority = ThreadPriority.Highest;
            Thread ssrControlThread1 = new Thread(() => SSRControl1(13));
            ssrControlThread1.Priority = ThreadPriority.Highest;
            ssrControlThread0.Start();
            ssrControlThread1.Start();

         //   PrometheusGuageWatts0 = Metrics.CreateGauge($"Watts0" , "watts");
          //  PrometheusGuageWatts1 = Metrics.CreateGauge($"Watts1" , "watts");

        }
        catch (Exception e)
        {
            Log.Error(e, "IO initialize");
        }
    }

    public void SetPWMFrequency(uint freq)
    {
    //    PWM0.Frequency = (int)freq;
    //    PWM1.Frequency = (int)freq;
    }

    public uint PercentagePWM0 { get; set; }
    public uint PercentagePWM1 { get; set; }

    public uint Watts1
    {
        get
        {
            return (uint)(LoadWatts * (PercentagePWM1 / 100f));
        }
    }

    public uint Watts0
    {
        get
        {
            return (uint)(LoadWatts * (PercentagePWM0 / 100f));
        }
    }


    public bool SetPercentage(uint pwm0Pc, uint pwm1Pc)
    {
        PercentagePWM0 = pwm0Pc;
        (var _MsOn0,var _MsOff0) = CalculateRatio(pwm0Pc, 50);
        MsOn0 = _MsOn0 * 10;
        MsOff0 = _MsOff0 * 10;

        PercentagePWM1 = pwm1Pc;
        (var _MsOn1,var _MsOff1) = CalculateRatio(pwm1Pc, 50);
        MsOn1 = _MsOn1 * 10;
        MsOff1 = _MsOff1 * 10;

        return true;
    }

    public async Task ScanTempBus(AnsiTelnetConsole ansiConsole = null)
    {
        try
        {
            Thermometers.Clear();
            foreach (string busId in OneWireBus.EnumerateBusIds())
            {
                OneWireBus bus = new(busId);
                var logString = $"Found bus [cyan]{bus.BusId}[/], scanning for devices ...";
                ansiConsole?.MarkupLine(logString);
                Log.Information(logString);
                await bus.ScanForDeviceChangesAsync();
                foreach (string devId in bus.EnumerateDeviceIds())
                {
                    OneWireDevice dev = new(busId, devId);
                    logString =
                        $"Found family [cyan]{dev.Family}[/] device [cyan]{dev.DeviceId}[/] on [cyan]{bus.BusId}[/]";
                    ansiConsole?.MarkupLine(logString);
                    Log.Information(logString);
                    if (OneWireThermometerDevice.IsCompatible(busId, devId))
                    {
                        OneWireThermometerDevice devTemp = new(busId, devId);
                        var thermometer = new clsThermometer(devTemp);
                        Thermometers.Add(thermometer);
                        switch (thermometer.Name)
                        {
                            case "Top":
                                TopTemp = thermometer;
                                Log.Information("Setting TopTemp");
                                break;
                            case "H0":
                                H0Temp = thermometer;
                                Log.Information("Setting H0Temp");
                                break;
                            case "H1":
                                H1Temp = thermometer;
                                Log.Information("Setting H1Temp");
                                break;
                        }

                        ansiConsole?.MarkupLine(
                            $"Temperature reported by device: {(await devTemp.ReadTemperatureAsync()).DegreesCelsius.ToString("F2")} \u00B0C");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex,"ScanTempBus Error");
        }
    }


    public static (int, int) CalculateRatio(double percentage, int totalSlots)
    {
        int activeSlots = (int)Math.Round(percentage * totalSlots / 100);
        int inactiveSlots = totalSlots - activeSlots;
        int gcd = GCD(activeSlots, inactiveSlots);

        return (activeSlots / gcd, inactiveSlots / gcd);
    }

    //Greatest Common Divisor
    private static int GCD(int a, int b)
    {
        return b == 0 ? a : GCD(b, a % b);
    }

    private void SSRControl0(int pin)
    {
        try
        {
            while (true)
            {
                if (MsOff0 > 0)
                {
#if !DEBUG
                controller.Write(pin, PinValue.Low);
#endif
                    Thread.Sleep(MsOff0);
                }

                if (MsOn0 > 0)
                {
#if !DEBUG
                controller.Write(pin, PinValue.High);
#endif
                    Thread.Sleep(MsOn0);
                }
                else
                {
#if !DEBUG
                controller.Write(pin, PinValue.Low);
#endif
                    Thread.Sleep(100);
                }
            }
        }catch(Exception e)
        {
            Log.Error(e, "SSRControl0 task failed");
        }
    }

    private void SSRControl1(int pin)
    {
        try
        {
            while (true)
            {
                if (MsOff1 > 0)
                {
#if !DEBUG
                controller.Write(pin, PinValue.Low);
#endif
                    Thread.Sleep(MsOff1);
                }

                if (MsOn1 > 0)
                {
#if !DEBUG
                controller.Write(pin, PinValue.High);
#endif
                    Thread.Sleep(MsOn1);
                }
                else
                {
#if !DEBUG
                controller.Write(pin, PinValue.Low);
#endif
                    Thread.Sleep(100);
                }
            }
        }
        catch (Exception e)
        {
            Log.Error(e, "SSRControl1 task failed");
        }
    }

    public async Task UpdateTemp()
    {

        foreach (var thermometer in Thermometers)
        {
            thermometer.Celsius = thermometer.Thermometer.ReadTemperatureAsync().Result.DegreesCelsius;
        }
    }

    public void Dispose()
    {
        controller.Dispose();
    }


    public void SetWatts(uint watts0, uint watts1)
    {
        if (watts0 > LoadWatts) watts0 = LoadWatts;
        if (watts1 > LoadWatts) watts1 = LoadWatts;

        var percent0 = ((float)watts0 / (float)LoadWatts) * 100 ;
        var percent1 = ((float)watts1 / (float)LoadWatts) * 100 ;
        SetPercentage((uint)percent0, (uint)percent1);

    }
}