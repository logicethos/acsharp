using System;
using System.Collections.Generic;

//curl -u "{API}:" "https://api.octopus.energy/v1/products/INTELLI-BB-VAR-23-03-01/electricity-tariffs/E-1R-INTELLI-BB-VAR-23-03-01-H/standard-unit-rates/"


namespace ACSharp.Octopus;

public class clsOctopusRates
{
    public int count { get; set; }
    public string next { get; set; }
    public object previous { get; set; }
    public List<Result> results { get; set; }

    public class Result
    {
        public double value_exc_vat { get; set; }
        public double value_inc_vat { get; set; }
        public DateTime valid_from { get; set; }
        public DateTime valid_to { get; set; }
    }

}