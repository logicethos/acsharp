using System;
using System.Threading;
using SpiDriver;

namespace ACSharp
{
    public partial class M90E32AS
    {
        
        private SpiDriver.Device spi;
        
        public void Connect(String port)
        {
            spi = new SpiDriver.Device(port);
            spi.Connect();
        }

        public void Disconnect()
        {
            spi.Close();
        }

        public byte[] Read16(byte register)
        {
            var dataout = new byte[2] {0x80, register};
            var datain = new byte[2];

            spi.SetOutput(Output.CS, true);
            spi.Write(dataout, 0, 2);

            spi.Read(datain, 0, 2);
            spi.SetOutput(Output.CS, false);
            
            return datain;
        }



        public byte[] Read32(byte register1,byte register2)
        {
            var datain = new byte[4];

            var first = Read16(register1);
            var second = Read16(register2);
            Array.Copy(first,0,datain,0,2);
            Array.Copy(second, 0, datain, 2, 2);

            return datain;
        }

        public UInt16 ReadUInt16(byte register1)
        {
            var data = Read16(register1);
            Array.Reverse(data);
            return BitConverter.ToUInt16(data);
        }
        
        public Int16 ReadInt16(byte register1)
        {
            var data = Read16(register1);
            Array.Reverse(data);
            return BitConverter.ToInt16(data);
        }
        
        public UInt32 ReadUInt32(byte register1, byte register2)
        {
            var data = Read32(register1, register2);
            Array.Reverse(data);
            return BitConverter.ToUInt32(data);
        }
        
        public Int32 ReadInt32(byte register1, byte register2)
        {
            var data = Read32(register1, register2);
            Array.Reverse(data);
            return BitConverter.ToInt32(data);
        }
        
        public void Write(byte register, byte[] data)
        {
            var dataout = new byte[4];
            dataout[0] = (byte) (register >> 8);
            dataout[1] = (byte) (register & 0xFF);
            Array.Copy(data,0,dataout,2,2);

            spi.SetOutput(Output.CS, true);
            spi.Write(dataout, 0, 4);
            spi.SetOutput(Output.CS, false);
        }

        public void WriteUInt16(byte register, UInt16 integer)
        {
            var data = BitConverter.GetBytes(integer);
            Array.Reverse(data);
            Write(register,data);
        }
        
    }
}