using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if !GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<Settings> GetSettings(Empty request, ServerCallContext context)
    {
        var settings = new ACSharpCommon.Proto.Settings();

        var MeterEn = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.MeterEn);
        settings.MeterEnable = MeterEn != 0;

        var ChMapI = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.ChannelMapI);
        settings.CurrentChannelMap = new Settings.Types.ADCMap()
        {
            CSRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[0] & (byte)0b00000111),
            BSRC = (Settings.Types.ADCMap.Types.Source)((ChMapI[1] & (byte)0b01110000) >> 4),
            ASRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[1] & (byte)0b00000111),
        };

        var ChMapU = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.ChannelMapU);
        settings.VoltageChannelMap = new Settings.Types.ADCMap()
        {
            CSRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[0] & (byte)0b00000111),
            BSRC = (Settings.Types.ADCMap.Types.Source)((ChMapI[1] & (byte)0b01110000) >> 4),
            ASRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[1] & (byte)0b00000111),
        };

        var SagPeekDetCfg = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.SagPeakDetCfg);
        settings.SagPeekDetCfg = new Settings.Types.SagPeakDetCfg()
        {
            PeakDetectorPeriodMs = SagPeekDetCfg[0],
            SagPeriodMs = SagPeekDetCfg[1],
        };

        var OVth = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.OVth);
        settings.OverVoltageThreshold = OVth;

        var ZXConfig = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.ZXConfig);
        settings.ZeroCrossingConfiguration = new Settings.Types.ZCrossConfig()
        {
            ZX2SRC = (Settings.Types.ZCrossConfig.Types.Source)((ZXConfig[0] & (byte)0b11100000) >> 5),
            ZX1SRC = (Settings.Types.ZCrossConfig.Types.Source)((ZXConfig[0] & (byte)0b00011100) >> 2),
            ZX0SRC = (Settings.Types.ZCrossConfig.Types.Source)(((ZXConfig[0] & (byte)0b00000011) << 1) |
                                                                (ZXConfig[1] & (byte)0b10000000) >> 7),
            ZX2Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b01100000) >> 5),
            ZX1Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b00011000) >> 3),
            ZX0Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b00000110) >> 1),
            DisableZCrossSignals = (ZXConfig[1] & (byte)0b00000001) > 0,
        };

        var SagTh = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.SagTh);
        settings.VoltageSagThreshold = SagTh;

        var PhaseLossTh = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.PhaseLossTh);
        settings.VoltagePhaseLosingThreashold = PhaseLossTh;

        var InWarnTh = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.InWarnTh);
        settings.NeutralCurrentWarningThreashold = InWarnTh;

        var OIth = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.OIth);
        settings.OverVoltageThreshold = OIth;

        var FreqLoTh = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.FreqLoTh);
        settings.LowThreasholdForFrequencyDetection = FreqLoTh;

        var FreqHiTh = Program.em.ReadUInt16((byte)M90E32AS.RegisterStatusSpecial.FreqHiTh);
        settings.HighThreasholdForFrequencyDetection = FreqHiTh;

        var PMPwrCtrl = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.PMPwrCtrl);
        settings.PartialMesurementModePowerControl = new Settings.Types.PMPwrCtrl()
        {
            PMPwrDownVch = (PMPwrCtrl[0] & (byte)0b00000001) > 0,
            ACTRLCLKGATE = (PMPwrCtrl[1] & (byte)0b00001000) > 0,
            DSPCLKGATE = (PMPwrCtrl[1] & (byte)0b00000100) > 0,
            MTMSCLKGATE = (PMPwrCtrl[1] & (byte)0b00000010) > 0,
            PMClkLow = (PMPwrCtrl[1] & (byte)0b00000001) > 0,
        };

        var IRQ0MergeCfg = Program.em.Read16((byte)M90E32AS.RegisterStatusSpecial.IRQ0MergeCfg);
        settings.IRQ0MetgeConfiguration = new Settings.Types.IRQ0MergeCfg()
        {
            WARNOR = (IRQ0MergeCfg[1] & (byte)0b00000010) > 0,
            IRQ1OR = (IRQ0MergeCfg[1] & (byte)0b00000001) > 0,
        };

        return Task.FromResult(settings);
    }

    public override Task<MsgReply> SetSettings(ACSharpCommon.Proto.Settings NewSettings, ServerCallContext context)
    {
        var msgReply = new MsgReply();
        int count = 0;

        var OldSettings = GetSettings(null, null).Result;

        if (OldSettings.MeterEnable != NewSettings.MeterEnable)
        {
            count++;
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.MeterEn, new byte[] { 0, 0x1 });
        }

        if (OldSettings.CurrentChannelMap.ASRC != NewSettings.CurrentChannelMap.ASRC ||
            OldSettings.CurrentChannelMap.BSRC != NewSettings.CurrentChannelMap.BSRC ||
            OldSettings.CurrentChannelMap.CSRC != NewSettings.CurrentChannelMap.CSRC)
        {
            var ChannelMapI = new byte[2];
            ChannelMapI[1] = (byte)NewSettings.CurrentChannelMap.CSRC;
            ChannelMapI[0] = (byte)((byte)NewSettings.CurrentChannelMap.BSRC << 4 |
                                    (byte)NewSettings.CurrentChannelMap.ASRC);
            count++;
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.ChannelMapI, ChannelMapI);
        }


        if (OldSettings.VoltageChannelMap.ASRC != NewSettings.VoltageChannelMap.ASRC ||
            OldSettings.VoltageChannelMap.BSRC != NewSettings.VoltageChannelMap.BSRC ||
            OldSettings.VoltageChannelMap.CSRC != NewSettings.VoltageChannelMap.CSRC)
        {
            var ChannelMapU = new byte[2];
            ChannelMapU[1] = (byte)NewSettings.VoltageChannelMap.CSRC;
            ChannelMapU[0] = (byte)((byte)NewSettings.VoltageChannelMap.BSRC << 4 |
                                    (byte)NewSettings.VoltageChannelMap.ASRC);
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.ChannelMapU, ChannelMapU);
            count++;
        }


        if (OldSettings.SagPeekDetCfg.SagPeriodMs != NewSettings.SagPeekDetCfg.SagPeriodMs ||
            OldSettings.SagPeekDetCfg.PeakDetectorPeriodMs != NewSettings.SagPeekDetCfg.PeakDetectorPeriodMs)
        {
            var SagPeakDetCfg = new byte[2];
            SagPeakDetCfg[0] = (byte)NewSettings.SagPeekDetCfg.PeakDetectorPeriodMs;
            SagPeakDetCfg[1] = (byte)NewSettings.SagPeekDetCfg.SagPeriodMs;
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.SagPeakDetCfg, SagPeakDetCfg);
            count++;
        }

        if (OldSettings.OverVoltageThreshold != NewSettings.OverVoltageThreshold)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.OVth,
                (UInt16)NewSettings.OverVoltageThreshold);
            count++;
        }


        if (OldSettings.ZeroCrossingConfiguration.ZX0Con != NewSettings.ZeroCrossingConfiguration.ZX0Con ||
            OldSettings.ZeroCrossingConfiguration.ZX1Con != NewSettings.ZeroCrossingConfiguration.ZX1Con ||
            OldSettings.ZeroCrossingConfiguration.ZX2Con != NewSettings.ZeroCrossingConfiguration.ZX2Con ||
            OldSettings.ZeroCrossingConfiguration.ZX0SRC != NewSettings.ZeroCrossingConfiguration.ZX0SRC ||
            OldSettings.ZeroCrossingConfiguration.ZX1SRC != NewSettings.ZeroCrossingConfiguration.ZX1SRC ||
            OldSettings.ZeroCrossingConfiguration.ZX2SRC != NewSettings.ZeroCrossingConfiguration.ZX2SRC ||
            OldSettings.ZeroCrossingConfiguration.DisableZCrossSignals !=
            NewSettings.ZeroCrossingConfiguration.DisableZCrossSignals
           )
        {
            var ZXConfig = new byte[2];

            ZXConfig[0] = (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX2SRC << 5);
            ZXConfig[0] |= (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX1SRC << 2);
            ZXConfig[0] |= (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX0SRC >> 1);
            ZXConfig[1] |= (byte)(((byte)NewSettings.ZeroCrossingConfiguration.ZX0SRC & (byte)0b00000001) << 7);

            ZXConfig[1] |= (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX2Con << 5);
            ZXConfig[1] |= (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX1Con << 3);
            ZXConfig[1] |= (byte)((byte)NewSettings.ZeroCrossingConfiguration.ZX0Con << 1);

            if (NewSettings.ZeroCrossingConfiguration.DisableZCrossSignals)
            {
                ZXConfig[1] |= 1;
            }
            else
            {
                ZXConfig[1] &= (byte)0xFE;
            }
        }

        if (NewSettings.VoltageSagThreshold != OldSettings.VoltageSagThreshold)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.SagTh,
                (UInt16)NewSettings.VoltageSagThreshold);
            count++;
        }

        if (NewSettings.VoltagePhaseLosingThreashold != OldSettings.VoltagePhaseLosingThreashold)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.PhaseLossTh,
                (UInt16)NewSettings.VoltagePhaseLosingThreashold);
            count++;
        }

        if (NewSettings.NeutralCurrentWarningThreashold != OldSettings.NeutralCurrentWarningThreashold)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.InWarnTh,
                (UInt16)NewSettings.NeutralCurrentWarningThreashold);
            count++;
        }


        if (NewSettings.OverVoltageThreshold != OldSettings.OverVoltageThreshold)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.OIth,
                (UInt16)NewSettings.OverVoltageThreshold);
            count++;
        }

        if (NewSettings.LowThreasholdForFrequencyDetection != OldSettings.LowThreasholdForFrequencyDetection)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.FreqLoTh,
                (UInt16)NewSettings.LowThreasholdForFrequencyDetection);
            count++;
        }

        if (NewSettings.HighThreasholdForFrequencyDetection != OldSettings.HighThreasholdForFrequencyDetection)
        {
            Program.em.WriteUInt16((byte)M90E32AS.RegisterStatusSpecial.FreqHiTh,
                (UInt16)NewSettings.HighThreasholdForFrequencyDetection);
            count++;
        }


        if (OldSettings.PartialMesurementModePowerControl.PMPwrDownVch !=
            NewSettings.PartialMesurementModePowerControl.PMPwrDownVch ||
            OldSettings.PartialMesurementModePowerControl.ACTRLCLKGATE !=
            NewSettings.PartialMesurementModePowerControl.ACTRLCLKGATE ||
            OldSettings.PartialMesurementModePowerControl.DSPCLKGATE !=
            NewSettings.PartialMesurementModePowerControl.DSPCLKGATE ||
            OldSettings.PartialMesurementModePowerControl.MTMSCLKGATE !=
            NewSettings.PartialMesurementModePowerControl.MTMSCLKGATE ||
            OldSettings.PartialMesurementModePowerControl.PMClkLow !=
            NewSettings.PartialMesurementModePowerControl.PMClkLow)
        {
            var PMPwrCtrl = new byte[2];
            if (NewSettings.PartialMesurementModePowerControl.PMPwrDownVch) PMPwrCtrl[0] |= (byte)0b00000001;
            if (NewSettings.PartialMesurementModePowerControl.ACTRLCLKGATE) PMPwrCtrl[1] |= (byte)0b00001000;
            if (NewSettings.PartialMesurementModePowerControl.DSPCLKGATE) PMPwrCtrl[1] |= (byte)0b00000100;
            if (NewSettings.PartialMesurementModePowerControl.MTMSCLKGATE) PMPwrCtrl[1] |= (byte)0b00000010;
            if (NewSettings.PartialMesurementModePowerControl.PMClkLow) PMPwrCtrl[1] |= (byte)0b00000001;
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.PMPwrCtrl, PMPwrCtrl);
            count++;
        }

        if (OldSettings.IRQ0MetgeConfiguration.WARNOR != NewSettings.IRQ0MetgeConfiguration.WARNOR ||
            OldSettings.IRQ0MetgeConfiguration.IRQ1OR != NewSettings.IRQ0MetgeConfiguration.IRQ1OR)
        {
            var IRQ0MergeCfg = new byte[2];
            if (NewSettings.IRQ0MetgeConfiguration.WARNOR) IRQ0MergeCfg[1] = (byte)0b00000010;
            if (NewSettings.IRQ0MetgeConfiguration.IRQ1OR) IRQ0MergeCfg[1] |= (byte)0b0000001;
            Program.em.Write((byte)M90E32AS.RegisterStatusSpecial.IRQ0MergeCfg, IRQ0MergeCfg);
            count++;
        }

        msgReply.Status = MsgReply.Types.Status.Ok;
        msgReply.Message = $"{count} changes";
        return Task.FromResult(msgReply);
    }
}
#endif