using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if !GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<VoltageAmpsRMS> GetPeekVoltageAmpsRMS(Empty request, ServerCallContext context)
    {
        var v1 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.UPeakA);
        var v2 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.UPeakB);
        var v3 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.UPeakC);

        var a1 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.IPeakA);
        var a2 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.IPeakB);
        var a3 = Program.em.ReadInt16((byte)M90E32AS.RegistersPeak.IPeakC);

        var voltageAmpsRMS = new ACSharpCommon.Proto.VoltageAmpsRMS()
        {
            AVolts = v1,
            BVolts = v2,
            CVolts = v3,
            AAmps = a1,
            BAmps = a2,
            CAmps = a3,
        };
        return Task.FromResult(voltageAmpsRMS);
    }

    public override Task<VoltageAmpsRMS> GetVoltageAmpsRMS(Empty request, ServerCallContext context)
    {
        var v1 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.UrmsA, (byte)M90E32AS.RegistersPower.UrmsALSB);
        var v2 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.UrmsB, (byte)M90E32AS.RegistersPower.UrmsBLSB);
        var v3 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.UrmsC, (byte)M90E32AS.RegistersPower.UrmsCLSB);

        var a1 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.IrmsA, (byte)M90E32AS.RegistersPower.IrmsALSB);
        var a2 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.IrmsB, (byte)M90E32AS.RegistersPower.IrmsBLSB);
        var a3 = Program.em.ReadUInt32((byte)M90E32AS.RegistersPower.IrmsC, (byte)M90E32AS.RegistersPower.IrmsCLSB);

        var voltageAmpsRMS = new ACSharpCommon.Proto.VoltageAmpsRMS()
        {
            AVolts = v1,
            BVolts = v2,
            CVolts = v3,
            AAmps = a1,
            BAmps = a2,
            CAmps = a3,
        };
        return Task.FromResult(voltageAmpsRMS);
    }
}
#endif