using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
#if !GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<PhaseInfo> GetPhaseInfo(Empty request, ServerCallContext context)
    {
        var phaseAngleA = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.PAngleA);
        var phaseAngleB = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.PAngleB);
        var phaseAngleC = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.PAngleC);
        var voltageAngleA = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.UangleA);
        var voltageAngleB = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.UangleB);
        var voltageAngleC = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.UangleC);
        var frequency = Program.em.ReadUInt16((byte)M90E32AS.RegistersPeak.Freq);

        var phaseInfo = new ACSharpCommon.Proto.PhaseInfo()
        {
            MeanPhaseAngle = new PhaseAngle()
            {
                A = phaseAngleA * 0.1,
                B = phaseAngleB * 0.1,
                C = phaseAngleC * 0.1,
            },
            VoltagePhaseAngle = new PhaseAngle()
            {
                A = voltageAngleA * 0.1,
                B = voltageAngleB * 0.1,
                C = voltageAngleC * 0.1,
            },
            Frequency = frequency * 0.01,
        };
        return Task.FromResult(phaseInfo);
    }
}
#endif