using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using System.Threading.Tasks;
using Proto.Authentication;
using Google.Protobuf;
using ACSharp;


namespace ACSharp.GRPC;

public class AuthenticationService : Proto.Authentication.ACSharpAuthentication.ACSharpAuthenticationBase
{

    public override Task<AuthenticationReply> NodeAuthenticate(NodeAuthenticationRequest request, ServerCallContext context)
    {
        var authenticationReply = JwtAuthenticationManager.AuthenticateNode(request);
        if (authenticationReply == null)
            throw new RpcException(new Status(StatusCode.Unauthenticated, "Invalid user Credentials"));

        //Program.DistributedProxyList.ConnectFrom(request);
        
        return Task.FromResult(authenticationReply);
    }
}