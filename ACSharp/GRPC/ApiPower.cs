using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
#if !GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<ReactivePower> GetReactivePower(Empty request, ServerCallContext context)
    {
        var W1 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.QmeanA,
            (byte)M90E32AS.RegistersPowerFactor.QmeanALSB);
        var W2 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.QmeanB,
            (byte)M90E32AS.RegistersPowerFactor.QmeanBLSB);
        var W3 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.QmeanC,
            (byte)M90E32AS.RegistersPowerFactor.QmeanCLSB);

        var reactivePower = new ACSharpCommon.Proto.ReactivePower()
        {
            AVar = W1 * 0.00032,
            BVar = W2 * 0.00032,
            CVar = W3 * 0.00032,
        };
        return Task.FromResult(reactivePower);
    }

    public override Task<ApparentPower> GetApparentPower(Empty request, ServerCallContext context)
    {
        var W1 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.SmeanA,
            (byte)M90E32AS.RegistersPowerFactor.SmeanALSB);
        var W2 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.SmeanB,
            (byte)M90E32AS.RegistersPowerFactor.SmeanBLSB);
        var W3 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.SmeanC,
            (byte)M90E32AS.RegistersPowerFactor.SmeanCLSB);

        var apparentPower = new ACSharpCommon.Proto.ApparentPower()
        {
            AVA = W1 * 0.00032,
            BVA = W2 * 0.00032,
            CVA = W3 * 0.00032,
        };
        return Task.FromResult(apparentPower);
    }

    public override Task<ActivePower> GetActivePower(Empty request, ServerCallContext context)
    {
        var W1 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.PmeanA,
            (byte)M90E32AS.RegistersPowerFactor.PmeanALSB);
        var W2 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.PmeanB,
            (byte)M90E32AS.RegistersPowerFactor.PmeanBLSB);
        var W3 = Program.em.ReadInt32((byte)M90E32AS.RegistersPowerFactor.PmeanC,
            (byte)M90E32AS.RegistersPowerFactor.PmeanCLSB);

        var activePower = new ACSharpCommon.Proto.ActivePower()
        {
            AWatts = W1 * 0.00032,
            BWatts = W2 * 0.00032,
            CWatts = W3 * 0.00032,
        };
        return Task.FromResult(activePower);
    }

    public override Task<PowerFactor> GetPowerFactor(Empty request, ServerCallContext context)
    {
        var p1 = Program.em.ReadInt16((byte)M90E32AS.RegistersPowerFactor.PFmeanA);
        var p2 = Program.em.ReadInt16((byte)M90E32AS.RegistersPowerFactor.PFmeanB);
        var p3 = Program.em.ReadInt16((byte)M90E32AS.RegistersPowerFactor.PFmeanC);

        var powerFactor = new ACSharpCommon.Proto.PowerFactor()
        {
            A = p1 * 0.001,
            B = p2 * 0.001,
            C = p3 * 0.001,
        };
        return Task.FromResult(powerFactor);
    }

    public override Task<Power> GetPower(Empty request, ServerCallContext context)
    {
        var power = new ACSharpCommon.Proto.Power()
        {
            ActivePower = GetActivePower(request, context).Result,
            ReactivePower = GetReactivePower(request, context).Result,
            ApparentPower = GetApparentPower(request, context).Result,
            PowerFactor = GetPowerFactor(request, context).Result,
        };

        return Task.FromResult(power);
    }

}
#endif
