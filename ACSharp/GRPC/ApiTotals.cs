using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if !GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<TotalEnergy> GetTotalEnergy(Empty request, ServerCallContext context)
    {
        var ActiveForwardA = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.APenergyA);
        var ActiveForwardB = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.APenergyB);
        var ActiveForwardC = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.APenergyC);
        var ActiveReverseA = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.ANenergyA);
        var ActiveReverseB = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.ANenergyB);
        var ActiveReverseC = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.ANenergyC);

        var ReactiveForwardA = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RPenergyA);
        var ReactiveForwardB = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RPenergyB);
        var ReactiveForwardC = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RPenergyC);
        var ReactiveReverseA = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RNenergyA);
        var ReactiveReverseB = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RNenergyB);
        var ReactiveReverseC = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.RNenergyC);

        var ApparentEnergyA = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.SenergyA);
        var ApparentEnergyB = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.SenergyB);
        var ApparentEnergyC = Program.em.ReadUInt16((byte)M90E32AS.RegisterEnergyRegisters.SenergyC);


        var totalEnergy = new ACSharpCommon.Proto.TotalEnergy()
        {
            ActiveEnergyForward = new ActivePower()
            {
                AWatts = ActiveForwardA * 0.1,
                BWatts = ActiveForwardB * 0.1,
                CWatts = ActiveForwardC * 0.1,
            },
            ActiveEnergyReverse = new ActivePower()
            {
                AWatts = ActiveReverseA * 0.1,
                BWatts = ActiveReverseB * 0.1,
                CWatts = ActiveReverseC * 0.1,
            },
            ReactivePowerForward = new ReactivePower()
            {
                AVar = ReactiveForwardA * 0.1,
                BVar = ReactiveForwardB * 0.1,
                CVar = ReactiveForwardC * 0.1,
            },
            ReactivePowerReverse = new ReactivePower()
            {
                AVar = ReactiveReverseA * 0.1,
                BVar = ReactiveReverseB * 0.1,
                CVar = ReactiveReverseC * 0.1,
            },

            ApparentPower = new ApparentPower()
            {
                AVA = ApparentEnergyA * 0.1,
                BVA = ApparentEnergyB * 0.1,
                CVA = ApparentEnergyC * 0.1,
            }

        };
        return Task.FromResult(totalEnergy);
    }
}
#endif