using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<ReactivePower> GetReactivePower(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();

        var reactivePower = new ACSharpCommon.Proto.ReactivePower()
        {
            AVar = rnd.NextDouble() * 1000,
            BVar = rnd.NextDouble() * 1000,
            CVar = rnd.NextDouble() * 1000,
        };
        return Task.FromResult(reactivePower);
    }

    public override Task<ApparentPower> GetApparentPower(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();
        
        var apparentPower = new ACSharpCommon.Proto.ApparentPower()
        {
            AVA = rnd.NextDouble() * 1000,
            BVA = rnd.NextDouble() * 1000,
            CVA = rnd.NextDouble() * 1000,
        };
        return Task.FromResult(apparentPower);
    }

    public override Task<ActivePower> GetActivePower(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();
        var activePower = new ACSharpCommon.Proto.ActivePower()
        {
            AWatts = rnd.NextDouble() * 1000,
            BWatts = rnd.NextDouble() * 1000,
            CWatts = rnd.NextDouble() * 1000,
        };
        return Task.FromResult(activePower);
    }

    public override Task<PowerFactor> GetPowerFactor(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();
        var powerFactor = new ACSharpCommon.Proto.PowerFactor()
        {
            A = rnd.NextDouble() * 1000,
            B = rnd.NextDouble() * 1000,
            C = rnd.NextDouble() * 1000,
        };
        return Task.FromResult(powerFactor);
    }

    public override Task<Power> GetPower(Empty request, ServerCallContext context)
    {
        var power = new ACSharpCommon.Proto.Power()
        {
            ActivePower = GetActivePower(request, context).Result,
            ReactivePower = GetReactivePower(request, context).Result,
            ApparentPower = GetApparentPower(request, context).Result,
            PowerFactor = GetPowerFactor(request, context).Result,
        };

        return Task.FromResult(power);
    }

}
#endif
