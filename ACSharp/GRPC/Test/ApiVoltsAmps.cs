using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<VoltageAmpsRMS> GetPeekVoltageAmpsRMS(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();

        var voltageAmpsRMS = new ACSharpCommon.Proto.VoltageAmpsRMS()
        {
            AVolts = 240 + rnd.NextSingle() * 5,
            BVolts = 240 + rnd.NextSingle() * 5,
            CVolts = 240 + rnd.NextSingle() * 5,
            AAmps = rnd.NextSingle() * 1000,
            BAmps = rnd.NextSingle() * 1000,
            CAmps = rnd.NextSingle() * 1000,
        };
        return Task.FromResult(voltageAmpsRMS);
    }

    public override Task<VoltageAmpsRMS> GetVoltageAmpsRMS(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();

        var voltageAmpsRMS = new ACSharpCommon.Proto.VoltageAmpsRMS()
        {
            AVolts = 240 + rnd.NextSingle() * 5,
            BVolts = 240 + rnd.NextSingle() * 5,
            CVolts = 240 + rnd.NextSingle() * 5,
            AAmps = rnd.NextSingle() * 1000,
            BAmps = rnd.NextSingle() * 1000,
            CAmps = rnd.NextSingle() * 1000,
        };
        return Task.FromResult(voltageAmpsRMS);
    }
}
#endif