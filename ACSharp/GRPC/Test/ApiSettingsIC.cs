using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<Settings> GetSettings(Empty request, ServerCallContext context)
    {
        var settings = new ACSharpCommon.Proto.Settings();

        var MeterEn = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.MeterEn);
        settings.MeterEnable = MeterEn != 0;

        var ChMapI = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.ChannelMapI);
        settings.CurrentChannelMap = new Settings.Types.ADCMap()
        {
            CSRC = Settings.Types.ADCMap.Types.Source.I0,
            BSRC = (Settings.Types.ADCMap.Types.Source)((ChMapI[1] & (byte)0b01110000) >> 4),
            ASRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[1] & (byte)0b00000111),
        };

        var ChMapU = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.ChannelMapU);
        settings.VoltageChannelMap = new Settings.Types.ADCMap()
        {
            CSRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[0] & (byte)0b00000111),
            BSRC = (Settings.Types.ADCMap.Types.Source)((ChMapI[1] & (byte)0b01110000) >> 4),
            ASRC = (Settings.Types.ADCMap.Types.Source)(ChMapI[1] & (byte)0b00000111),
        };

        var SagPeekDetCfg = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.SagPeakDetCfg);
        settings.SagPeekDetCfg = new Settings.Types.SagPeakDetCfg()
        {
            PeakDetectorPeriodMs = SagPeekDetCfg[0],
            SagPeriodMs = SagPeekDetCfg[1],
        };

        var OVth = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.OVth);
        settings.OverVoltageThreshold = OVth;

        var ZXConfig = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.ZXConfig);
        settings.ZeroCrossingConfiguration = new Settings.Types.ZCrossConfig()
        {
            ZX2SRC = (Settings.Types.ZCrossConfig.Types.Source)((ZXConfig[0] & (byte)0b11100000) >> 5),
            ZX1SRC = (Settings.Types.ZCrossConfig.Types.Source)((ZXConfig[0] & (byte)0b00011100) >> 2),
            ZX0SRC = (Settings.Types.ZCrossConfig.Types.Source)(((ZXConfig[0] & (byte)0b00000011) << 1) |
                                                                (ZXConfig[1] & (byte)0b10000000) >> 7),
            ZX2Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b01100000) >> 5),
            ZX1Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b00011000) >> 3),
            ZX0Con = (Settings.Types.ZCrossConfig.Types.Config)((ZXConfig[1] & (byte)0b00000110) >> 1),
            DisableZCrossSignals = (ZXConfig[1] & (byte)0b00000001) > 0,
        };

        var SagTh = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.SagTh);
        settings.VoltageSagThreshold = SagTh;

        var PhaseLossTh = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.PhaseLossTh);
        settings.VoltagePhaseLosingThreashold = PhaseLossTh;

        var InWarnTh = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.InWarnTh);
        settings.NeutralCurrentWarningThreashold = InWarnTh;

        var OIth = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.OIth);
        settings.OverVoltageThreshold = OIth;

        var FreqLoTh = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.FreqLoTh);
        settings.LowThreasholdForFrequencyDetection = FreqLoTh;

        var FreqHiTh = Program.em.ReadUInt16((UInt16)M90E32AS.RegisterStatusSpecial.FreqHiTh);
        settings.HighThreasholdForFrequencyDetection = FreqHiTh;

        var PMPwrCtrl = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.PMPwrCtrl);
        settings.PartialMesurementModePowerControl = new Settings.Types.PMPwrCtrl()
        {
            PMPwrDownVch = (PMPwrCtrl[0] & (byte)0b00000001) > 0,
            ACTRLCLKGATE = (PMPwrCtrl[1] & (byte)0b00001000) > 0,
            DSPCLKGATE = (PMPwrCtrl[1] & (byte)0b00000100) > 0,
            MTMSCLKGATE = (PMPwrCtrl[1] & (byte)0b00000010) > 0,
            PMClkLow = (PMPwrCtrl[1] & (byte)0b00000001) > 0,
        };

        var IRQ0MergeCfg = Program.em.Read16((UInt16)M90E32AS.RegisterStatusSpecial.IRQ0MergeCfg);
        settings.IRQ0MetgeConfiguration = new Settings.Types.IRQ0MergeCfg()
        {
            WARNOR = (IRQ0MergeCfg[1] & (byte)0b00000010) > 0,
            IRQ1OR = (IRQ0MergeCfg[1] & (byte)0b00000001) > 0,
        };

        return Task.FromResult(settings);
    }

    public override Task<MsgReply> SetSettings(ACSharpCommon.Proto.Settings NewSettings, ServerCallContext context)
    {
        var msgReply = new MsgReply();
        int count = 0;

        msgReply.Status = MsgReply.Types.Status.Ok;
        msgReply.Message = $"{count} changes";
        return Task.FromResult(msgReply);
    }
}
#endif