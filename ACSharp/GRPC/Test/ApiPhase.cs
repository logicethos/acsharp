using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<PhaseInfo> GetPhaseInfo(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();
        
        var phaseInfo = new ACSharpCommon.Proto.PhaseInfo()
        {
            MeanPhaseAngle = new PhaseAngle()
            {
                A = rnd.NextDouble() * 360,
                B = rnd.NextDouble() * 360,
                C = rnd.NextDouble() * 360,
            },
            VoltagePhaseAngle = new PhaseAngle()
            {
                A = rnd.NextDouble() * 360,
                B = rnd.NextDouble() * 360,
                C = rnd.NextDouble() * 360,
            },
            Frequency = 50d + rnd.NextDouble(),
        };
        return Task.FromResult(phaseInfo);
    }
}
#endif