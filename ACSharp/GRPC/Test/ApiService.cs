using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public static ApiService instance;


    public ApiService()
    {
        instance = this;
    }

    public override Task<MsgReply> SetPWM(PWM request, ServerCallContext context)
    {
        return Task.FromResult(new MsgReply()
        {
            Status = MsgReply.Types.Status.Ok
        });
    }

    public override Task<PWM> GetPWM(Channel request, ServerCallContext context)
    {
        return base.GetPWM(request, context);
    }


}
#endif