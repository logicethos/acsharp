using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;

#if GRPCTEST
namespace ACSharp.GRPC;

public partial class ApiService : ACSharpAPI.ACSharpAPIBase
{
    public override Task<TotalEnergy> GetTotalEnergy(Empty request, ServerCallContext context)
    {
        Random rnd = new Random();

        var totalEnergy = new ACSharpCommon.Proto.TotalEnergy()
        {
            ActiveEnergyForward = new ActivePower()
            {
                AWatts = rnd.NextDouble() * 1000,
                BWatts = rnd.NextDouble() * 1000,
                CWatts = rnd.NextDouble() * 1000,
            },
            ActiveEnergyReverse = new ActivePower()
            {
                AWatts = rnd.NextDouble() * 1000,
                BWatts = rnd.NextDouble() * 1000,
                CWatts = rnd.NextDouble() * 1000,
            },
            ReactivePowerForward = new ReactivePower()
            {
                AVar = rnd.NextDouble() * 1000,
                BVar = rnd.NextDouble() * 1000,
                CVar = rnd.NextDouble() * 1000,
            },
            ReactivePowerReverse = new ReactivePower()
            {
                AVar = rnd.NextDouble() * 1000,
                BVar = rnd.NextDouble() * 1000,
                CVar = rnd.NextDouble() * 1000,
            },

            ApparentPower = new ApparentPower()
            {
                AVA = rnd.NextDouble() * 1000,
                BVA = rnd.NextDouble() * 1000,
                CVA = rnd.NextDouble() * 1000,
            }

        };
        return Task.FromResult(totalEnergy);
    }
}
#endif