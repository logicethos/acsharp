using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace ACSharp.TaskManager;

static public class clsTaskManager
{
    static private List<ITaskManagerTask> Tasks = new List<ITaskManagerTask>();
    static readonly object _lock = new object();


    static clsTaskManager()
    {
        LoadStartupTasks();
    }

    static public void LoadStartupTasks()
    {
        var type = typeof(ITaskManagerTask);
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(p => type.IsAssignableFrom(p)&& !p.IsInterface);

        lock (_lock)
        {
            foreach (var taskType in types)
            {
                ITaskManagerTask newtask = (ITaskManagerTask)Activator.CreateInstance(taskType);
                if (newtask.RunOnStartup) Tasks.Add(newtask);
            }
        }
    }

    static public void Run()
    {
        ITaskManagerTask[] list;

        lock (_lock)
        {
            var x = Tasks.RemoveAll(x => x.DestroyAfter != null && x.DestroyAfter < DateTime.UtcNow);
            list = Tasks.Where(x => x.StartAfter < DateTime.UtcNow).ToArray();
        }

        if (list.Length > 0)
        {
            CancellationToken token = new CancellationToken();
            var options = new ParallelOptions { MaxDegreeOfParallelism = 3, CancellationToken = token };
            Parallel.ForEachAsync<ITaskManagerTask>(list, options, async (task, token) =>
            {
                try
                {
                    Log.Information($"TASK: Running task {task.GetType()}");
                    await task.Run();
                    if (task.Interval != null)
                    {
                        if (task.StartAfter < DateTime.UtcNow) //Check that StartAfter hasn't been changed by the task
                        {
                            task.StartAfter = DateTime.UtcNow.Add(task.Interval.Value);
                        }
                    }
                    else
                    {
                        lock (_lock) Tasks.Remove(task);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e,"Timed Task Failed");
                }
            });
        }
    }


    static public void AddTask<T>(ITaskManagerTask task)
    {
        lock (_lock)
        {
            if (!task.AllowDuplicates)
            {
                if (Tasks.OfType<T>().Any())
                {
                    Log.Information("Adding Task: {name} Duplicate",typeof(T).FullName);
                    return;
                }
            }
            Log.Information("Adding Task: {name}",typeof(T).FullName);
            Tasks.Add(task);
        }
    }

    static public void RemoveTask(ITaskManagerTask task)
    {
        lock (_lock)
        {
            if (Tasks.Contains(task)) Tasks.Remove(task);
        }
    }


    public static void RemoveAllTasks()
    {
        lock (_lock) Tasks.Clear();
    }
}