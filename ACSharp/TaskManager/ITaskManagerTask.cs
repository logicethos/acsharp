using System;
using System.Threading.Tasks;

namespace ACSharp.TaskManager;

public interface ITaskManagerTask
{
    enum TaskStatus
    {
        Waiting,
        Running
    }

    public bool RunOnStartup { get; }
    public bool AllowDuplicates { get; }
    public TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter  { get; }
    public TimeSpan? Interval  { get; }

    public Task Run();
}