using System;
using System.Threading.Tasks;
using ConsoleServerDocker;
using Serilog;

namespace ACSharp.TaskManager;

public class SetupTempSensors : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; private set; }
    public TimeSpan? Interval { get; }

    private TimeSpan StartTime = new TimeSpan(0, 30, 0);
    private TimeSpan EndTime = new TimeSpan(7, 29, 0);

    public SetupTempSensors()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromMinutes(2);
    }

    public async Task Run()
    {
        if (Program.PiIO.Thermometers.Count == 0)
        {
            Program.PiIO.ScanTempBus().Wait();
        }

        if (Program.PiIO.Thermometers.Count > 0)
        {
            DestroyAfter = DateTime.UtcNow;
        }
        else
        {
            StartAfter = DateTime.UtcNow;
        }
    }

}