using System;
using System.Threading.Tasks;
using ConsoleServerDocker;
using Serilog;

namespace ACSharp.TaskManager;

public class Tempreture : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }

    static Task updateTask = null;

    public Tempreture()
    {
        StartAfter = DateTime.UtcNow.AddSeconds(10);
        Interval = TimeSpan.FromMinutes(1);
    }

    public async Task Run()
    {

        if (updateTask?.IsCompleted ?? true)
        {
            updateTask = Program.PiIO.UpdateTemp();
            Program.Influx.UpdateDB();
        }
        else
        {
            Log.Information("Temp update task is still running");
        }


    }

}