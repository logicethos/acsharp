using System;
using System.Threading.Tasks;
using ConsoleServerDocker;
using Serilog;

namespace ACSharp.TaskManager;

public class Weather : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; private set;}

    private TimeSpan StartTime = new TimeSpan(0, 30, 0);
    private TimeSpan EndTime = new TimeSpan(7, 29, 0);

    public Weather()
    {
        StartAfter = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day,DateTime.UtcNow.Hour,0,0);
        Interval = TimeSpan.FromHours(2);
    }

    public async Task Run()
    {

        if (StartAfter.Value.Hour >= 22 || StartAfter.Value.Hour <= 7)
        {
            var weather = await clsMetOffice.GetUVIndexCount(StartAfter.Value.AddHours(2));
            Program.WeatherGuageUVCount.Set(weather);
            Interval = TimeSpan.FromHours(2);
        }
        else
        {
            var uvIndex = await clsMetOffice.GetUVIndexNow();
            if (uvIndex > 0) Program.WeatherGuageUV.Set(uvIndex);
            Interval = TimeSpan.FromHours(1);
        }

    }

}