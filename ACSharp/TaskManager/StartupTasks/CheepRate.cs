using System;
using System.Threading.Tasks;
using ConsoleServerDocker;
using Serilog;

namespace ACSharp.TaskManager;

public class CheepRate : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }

    private TimeSpan StartTime = new TimeSpan(23, 30, 0);
    private TimeSpan EndTime = new TimeSpan(5, 29, 0);

    public CheepRate()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromMinutes(1);
    }

    public async Task Run()
    {
        if ((DateTime.UtcNow.TimeOfDay >= StartTime || DateTime.UtcNow.TimeOfDay < EndTime) && Program.PiIO.TopTemp.Celsius > 0)
        {
             Program.HotWaterTimer = true;
        }
        else
        {
             Program.HotWaterTimer = false;
        }
    }

}