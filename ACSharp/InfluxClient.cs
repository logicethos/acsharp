using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Core.Flux.Domain;
using InfluxDB.Client.Writes;
using ACSharpCommon.Proto;
using Serilog;
using Field = Google.Protobuf.WellKnownTypes.Field;

namespace ACSharp;



public class InfluxClient : IDisposable
{

    InfluxDBClient? influx;
    string _influxUrl;
    string APIToken;
    WriteApiAsync _writeApi;
    string Organisation;


    public InfluxClient(string InfluxURL,string organisation, string apiToken)
    {
        _influxUrl = InfluxURL;
        APIToken = apiToken;
        Organisation = organisation;
    }


    public void Open()
    {
        try
        {
            influx?.Dispose();
            Log.Information($"InfluxClient: Opening {_influxUrl} Org:{Organisation} Token {APIToken.Substring(0, 6)}");
            influx = InfluxDBClientFactory.Create(_influxUrl, APIToken);
            _writeApi = influx.GetWriteApiAsync();

            var bucketsApi = influx.GetBucketsApi();
            var buckets = bucketsApi.FindBucketsAsync().Result;

            foreach (var bucket in buckets)
            {
                Log.Information($"Bucket found: {bucket.Name} Created: {bucket.CreatedAt:yyyy MMMM dd}");

            }

            if (!buckets.Any(bucket => bucket.Name == "hotwater"))
            {
                Log.Error("WARNING: Bucket not found!");
            }

        }
        catch (Exception e)
        {
            Log.Error(e, "InfluxDB Open Error");
        }
    }

    public void WritePoint(PointData point,string bucket)
    {
        _writeApi.WritePointAsync(point,bucket,Organisation);
    }


    public void UpdateDB()
    {
        try
        {

            var pointList = new List<PointData>();
            foreach (var thermometer in Program.PiIO.Thermometers)
            {
                if (thermometer.Celsius != 0)
                {
                    var point = PointData.Measurement("thermometers").Field(thermometer.Name, Math.Round(thermometer.Celsius,2)).Timestamp(DateTime.UtcNow, WritePrecision.Ns);
                    pointList.Add(point);
                }
            }


            var point2 = PointData.Measurement("heaters").Field("Watts0", Program.PiIO.Watts0)
                 .Field("Watts1", Program.PiIO.Watts1)
                 .Timestamp(DateTime.UtcNow, WritePrecision.Ns);
            pointList.Add(point2);

            _writeApi.WritePointsAsync(pointList,"hotwater",Organisation);

        }
        catch (Exception e)
        {
            Log.Error(e, "Influx Write Error");
        }
    }

    public async Task<Dictionary<string, object>> GetBucketStatistics(string bucketName)
    {
        var statistics = new Dictionary<string, object>();
        QueryApi _queryApi = influx.GetQueryApi();
        try
        {
            var query = $"from(bucket: \"{bucketName}\") |> range(start: 0) |> keep(columns: [\"_value\", \"_field\", \"_time\"])";
            var tables = await _queryApi.QueryAsync(query, Organisation);

            var points = tables.SelectMany(table => table.Records).ToList();

            if (points.Count == 0)
            {
                Log.Information("No points found in the bucket.");
                statistics["PointCount"] = 0;
                return statistics;
            }

            statistics["PointCount"] = points.Count;
            statistics["EarliestTimestamp"] = points.Min(p => p.GetTime());
            statistics["LatestTimestamp"] = points.Max(p => p.GetTime());

            var fields = points
                .Where(p => double.TryParse(p.GetValue()?.ToString(), out _))
                .GroupBy(p => p.GetField())
                .ToDictionary(
                    g => g.Key,
                    g => new
                    {
                        Min = g.Min(p => Convert.ToDouble(p.GetValue())),
                        Max = g.Max(p => Convert.ToDouble(p.GetValue())),
                        Avg = g.Average(p => Convert.ToDouble(p.GetValue()))
                    });

            statistics["Fields"] = fields;

            Log.Information($"Bucket Statistics: {string.Join(", ", statistics.Select(kv => $"{kv.Key}: {kv.Value}"))}");
        }
        catch (Exception e)
        {
            Log.Error(e, "Error retrieving bucket statistics.");
        }

        return statistics;
    }

    public void Dispose()
    {
        influx?.Dispose();
    }
}