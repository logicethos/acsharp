using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ACSharp;


public class clsMetOfficeData
{

    public enum WeatherCode
    {
        ClearNight = 0,
        SunnyDay = 1,
        PartlyCloudyNight = 2,
        PartlyCloudyDay = 3,
        Mist = 5,
        Fog = 6,
        Cloudy = 7,
        Overcast = 8,
        LightRainShowerNight = 9,
        LightRainShowerDay = 10,
        Drizzle = 11,
        LightRain = 12,
        HeavyRainShowerNight = 13,
        HeavyRainShowerDay = 14,
        HeavyRain = 15,
        SleetShowerNight = 16,
        SleetShowerDay = 17,
        Sleet = 18,
        HailShowerNight = 19,
        HailShowerDay = 20,
        Hail = 21,
        LightSnowShowerNight = 22,
        LightSnowShowerDay = 23,
        LightSnow = 24,
        HeavySnowShowerNight = 25,
        HeavySnowShowerDay = 26,
        HeavySnow = 27,
        ThunderShowerNight = 28,
        ThunderShowerDay = 29,
        Thunder = 30
    }


    public string type { get; set; }
    public List<Feature> features { get; set; }



    public class Feature
    {
        public string type { get; set; }
        public Geometry geometry { get; set; }
        public Properties properties { get; set; }
    }

    public class Geometry
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Properties
    {
        public double requestPointDistance { get; set; }
        public string modelRunDate { get; set; }
        public List<TimeSeries> timeSeries { get; set; }
    }



    public class TimeSeries
    {
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime time { get; set; }
        public double screenTemperature { get; set; }
        public double maxScreenAirTemp { get; set; }
        public double minScreenAirTemp { get; set; }
        public double screenDewPointTemperature { get; set; }
        public double feelsLikeTemperature { get; set; }
        public double windSpeed10m { get; set; }
        public int windDirectionFrom10m { get; set; }
        public double windGustSpeed10m { get; set; }
        public double max10mWindGust { get; set; }
        public int visibility { get; set; }
        public double screenRelativeHumidity { get; set; }
        public int mslp { get; set; }
        public uint uvIndex { get; set; }
        public int significantWeatherCode { get; set; }
        public double precipitationRate { get; set; }
        public double totalPrecipAmount { get; set; }
        public int totalSnowAmount { get; set; }
        public int probOfPrecipitation { get; set; }
    }

}

public class DateTimeConverter : JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateTime.Parse(reader.GetString());
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString("yyyy-MM-ddTHH:mm:ssZ"));
    }
}