using System;
using System.Net;
using ACSharp;
using App.Metrics.Formatters.Prometheus;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace ACSharp;

public class clsWebHostBuilder :  HostBuilder
{

    public clsWebHostBuilder()
    {

        /*var logger = new LoggerConfiguration()
          //  .ReadFrom.Configuration(this.Configuration)
            .Enrich.FromLogContext()
            .CreateLogger();*/
        this.UseSerilog();
        /*this.UseMetricsWebTracking()
            .UseMetricsEndpoints(options =>
            {
                options.MetricsEndpointOutputFormatter = new MetricsPrometheusProtobufOutputFormatter();
            });*/

        this.ConfigureWebHost(webHost =>
        {
            Log.Information($"Configuring Kestrel");            
            webHost.UseKestrel(k =>
            {
                Log.Information($"Configuring Kestrel LINE 2");
                k.ConfigureHttpsDefaults(h =>
                {
                    h.ClientCertificateMode = ClientCertificateMode.AllowCertificate;
                    h.ClientCertificateValidation = (certificate, chain, errors) =>
                    {
                        return true; //certificate.Issuer == serverCert.Issuer;
                    };
                });

                var ssCert = SelfSignedCertificate.GetSelfSignedCertificate();

                /*//P2P port
                Log.Information($"Adding Port: {Program.Settings.P2PPort} P2P");
                k.Listen(IPAddress.Any, (int)Program.Settings.P2PPort, options =>
                {
                    options.UseHttps(ssCert, options =>
                    {
                        options.AllowAnyClientCertificate();
                    });

                });*/


                try
                {
                    Log.Information($"Program.Settings.WWWPort={Program.Settings.WWWPort}");
                    if (Program.Settings.WWWPort > 0)
                    {
                        k.Listen(IPAddress.Any, (int)Program.Settings.WWWPort, options =>
                        {
                        
                            if ((Program.Settings.DomainNames?.Count ?? 0) == 0)
                            {
                                Log.Information("Adding Port: {port} (Self Cert)", Program.Settings.WWWPort);

                                if (Program.SSLCert != null) options.UseHttps(Program.SSLCert);
#if !DEBUG
                                else options.UseHttps(ssCert);
#endif

                            }
                            else
                            {
                                options.UseHttps(o =>
                                {
                                    Log.Information("Adding Port: {port} (Certified)", Program.Settings.WWWPort);

                                    if (Program.SSLCert != null) o.ServerCertificate = Program.SSLCert;
#if !DEBUG
                                    else o.UseLettuceEncrypt(k.ApplicationServices);
#endif
                                });
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e,"Setup WWW");
                }

            });
            webHost.UseStartup<Startup>();
        });
    }
}