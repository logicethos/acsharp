using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using ConsoleServer;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using ILogger = Serilog.ILogger;

namespace ACSharp.TelnetConsole;

public partial class clsTelnetAppSession
{
    public  TelnetSession TelnetSession { get; init; }
    public AnsiTelnetConsole AnsiConsole => TelnetSession.AnsiConsole;
    private ILogger _logger;

    public clsTelnetAppSession(TelnetSession telnetSession , ILogger logger)
    {
        TelnetSession = telnetSession;
        _logger = logger;
    }

    public void Run()
    {
        try
        {

            bool result;
            do
            {
                //  var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;

                var menulist = new List<SelectionFunction<bool>>();


                menulist.Add(new SelectionFunction<bool>($"Grid display", () =>
                {

                    if (Program.PiIO?.Thermometers == null)
                    {
                        AnsiConsole.WriteLine("Oops we have a NULL");
                        End();
                        return true;
                    }

                    var table = new Table().LeftAligned();

                    AnsiConsole.Live(table)
                        .Start(ctx =>
                        {
                            table.AddColumn("Grid");
                            table.AddColumn("Watts 0");
                            table.AddColumn("Watts 1");

                            foreach (var piIoThermometer in Program.PiIO.Thermometers)
                            {
                                table.AddColumn(piIoThermometer.Name);
                            }

                            ctx.Refresh();

                            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

                        using (var run = Task.Run(() =>
                        {
                            while (!cancellationTokenSource.IsCancellationRequested)
                            {
                                var rowStrings = new List<string>() {Program.GridWatts.ToString(), $"{Program.PiIO.Watts0}", $"{Program.PiIO.Watts1}"};
                                foreach (var piIoThermometer in Program.PiIO.Thermometers)
                                {
                                    rowStrings.Add($"{piIoThermometer.Celsius:0.0}");
                                }

                                table.AddRow(rowStrings.ToArray());

                                if (table.Rows.Count > 10) table.RemoveRow(0);
                                ctx.Refresh();
                                Task.Delay(3000,cancellationTokenSource.Token).Wait();
                            }
                        },cancellationTokenSource.Token))
                        {
                            AnsiConsole.Input.ReadKey(false);
                            cancellationTokenSource.Cancel();
                            Task.Delay(500).Wait();
                        }
                        });

                    return true;
                }));



                menulist.Add(new SelectionFunction<bool>($"Scan Thermometer", () =>
                {
                    Program.PiIO.ScanTempBus(AnsiConsole).Wait();
                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Set HotWater Percent", () =>
                {
                    var hw0 = AnsiConsole.Ask<uint>("HW 0 Percent?");
                    var hw1 = AnsiConsole.Ask<uint>("HW 1 Percent?");
                    Program.PiIO.SetPercentage(hw0, hw1);
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Set HotWater Frequency", () =>
                {
                    var hwf = AnsiConsole.Ask<uint>("HW Frequency?");

                    Program.PiIO.SetPWMFrequency(hwf);
                    return true;
                }));



                menulist.Add(new SelectionFunction<bool>($"Disable HotWater Timer", () =>
                {
                    Program.DisableHotWaterTimer = YesNo("Disable Hot Water?");

                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"InfluxDB stats", () =>
                {


                    var stats = Program.Influx.GetBucketStatistics("hotwater").Result;

                    foreach (var stat in stats)
                    {
                        if (stat.Value is Dictionary<string, object> fields)
                        {
                            AnsiConsole.WriteLine($"{stat.Key}:");
                            foreach (var field in fields)
                            {
                                if (field.Value is Dictionary<string, double> fieldStats)
                                {
                                    AnsiConsole.WriteLine($"  {field.Key}:");
                                    foreach (var fieldStat in fieldStats)
                                    {
                                        AnsiConsole.WriteLine($"    {fieldStat.Key}: {fieldStat.Value}");
                                    }
                                }
                                else
                                {
                                    AnsiConsole.WriteLine($"  {field.Key}: {field.Value}");
                                }
                            }
                        }
                        else
                        {
                            AnsiConsole.WriteLine($"{stat.Key}: {stat.Value}");
                        }
                    }

                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>(Program.RestartRequired ? "[yellow]ACSharp Restart Required[/]" : Program.NewVersion ? "[yellow]Update ACSharp[/]" : "Restart ACSharp", () =>
                {
                    /*if (Program.SaveChanges)
                    {
                        clsSettings.Save();
                        Program.SaveChanges = false;
                    }*/
                    var container = Program.DockerManager.UpgradeContainer(AnsiConsole).Result;
                    AnsiConsole.MarkupLine("[yellow]Waiting for restart[/]");
                    Thread.Sleep(10000);
                    return true;

                }));
                /*menulist.Add(new SelectionFunction<bool>("Debug", () =>
                {
                    new clsDebug(this).Debug();
                    return true;
                }));*/

                /*
                if (Program.SaveChanges && !Program.RestartRequired)
                {
                    menulist.Add(new SelectionFunction<bool>($"[yellow]Save Changes[/]", () =>
                    {
                        clsSettings.Save();
                        Program.SaveChanges = false;
                        return true;
                    }));
                }
                */

                menulist.Add(new SelectionFunction<bool>("Exit", () => { return false; }));

                ShowScreenHeader();

                var option = AnsiConsole.Prompt(
                    new SelectionPrompt<SelectionFunction<bool>>()
                        .Title("[magenta]Main Menu[/]")
                        .PageSize(20)
                        .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                        .AddChoices(menulist));

                result = option.MenuAction();

            } while (result);

        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
        }

    }

    public async Task ShowScreenHeader()
    {

        AnsiConsole.Clear(true);

        var rule = new Rule("[white]ACSharp Console[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.WriteLine();
        var grid = new Grid();
        grid.AddColumn();
        grid.AddColumn();
        grid.AddColumn();
        if (Program.PiIO != null)
        {

            grid.AddRow("HW 0: ", $"[green]{Program.PiIO?.MsOn0}[/]", $"[green]{Program.PiIO?.MsOff0}[/]");
            grid.AddRow("HW 1: ", $"[green]{Program.PiIO?.MsOn1}[/]", $"[green]{Program.PiIO?.MsOff1}[/]");


//            grid.AddRow("HW 0: ", $"[green]{Program.PiIO?.PWM0Frequency}[/]hz", $"[green]{Program.PiIO?.PWM0DutyCycle:P0}[/]");
 //           grid.AddRow("HW 1: ", $"[green]{Program.PiIO?.PWM1Frequency}[/]hz", $"[green]{Program.PiIO?.PWM1DutyCycle:P0}[/]");
            grid.AddRow("Timer", $"{Program.HotWaterTimer}", $"[red]{(Program.DisableHotWaterTimer ? "Disabled" : "")}[/]");


        }
        else
        {
            AnsiConsole.Write("NO PIO");
        }

        AnsiConsole.Write(grid);
        var rule2 = new Rule( $"[blue]{AssemblyGitCommitTag.GetValueOrTime()}[/]");
        rule2.RuleStyle("blue");
        rule2.RightAligned();
        AnsiConsole.Write(rule2);
    }

    public string MenuActiveMarkup(string input, bool active)
    {
        if (active) return input;
        return $"[grey]{input}[/]";
    }

    public bool AreYouSure(string prompt = "Confirm")
    {
        var rule = new Rule($"[white]{prompt}[/]");
        rule.RuleStyle("red");
        rule.LeftAligned();
        AnsiConsole.Write(rule);
        
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));
        
        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title("Are you sure")
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

    public bool YesNo(string prompt = "Select")
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }


    public void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }
}
