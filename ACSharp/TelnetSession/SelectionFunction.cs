using System;
using Serilog.Events;

namespace ACSharp.TelnetConsole;

public enum SelectEnum
{
    Selection,
    New,
    Exit,
    Off,
    Enable,
    Disable,
    Reset,
    Auto,
    SubMenu
}

public record SelectionFunction<T>(string Prompt, Func<T> MenuAction)
{
    public override string ToString()
    {
        return Prompt;
    }
}

public record SelectionItemT<T>(string Prompt, SelectEnum MenuAction, T Item = null) where T : class
{
    public override string ToString()
    {
        return Prompt;
    }
}


public class SelectionLoggerItem
{
    public LogEventLevel LogLevel { get; init; }
    private string menuText;
    public SelectEnum Selection { get; init; }
    
    public SelectionLoggerItem(string text, SelectEnum select, bool highlight=false)
    {
        if (highlight)
        {
            menuText = $"[yellow]{text}[/]";
        }
        else
        {
            menuText = text;
        }
        Selection = select;
    }

    public SelectionLoggerItem(LogEventLevel logLevel, bool highlight=false)
    {
        if (highlight)
        {
            menuText = $"[yellow]{Enum.GetName<LogEventLevel>(logLevel)}[/]";    
        }
        else
        {
            menuText = Enum.GetName<LogEventLevel>(logLevel);
        }
        LogLevel = logLevel;
        Selection = SelectEnum.Selection;
    }
    
    public override string ToString()
    {
        return menuText;
    }
}