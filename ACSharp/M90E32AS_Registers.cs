using System;

namespace ACSharp
{
    public partial class M90E32AS
    {
        /// <summary>
        /// Status and Special Register
        /// </summary>
        public enum RegisterStatusSpecial : Byte
        {
            ///<summary>R/W Metering Enable P 41</summary>
            MeterEn = 0x00,
            ///<summary>R/W Current Channel Mapping Configuration P 42</summary>
            ChannelMapI = 0x01,
            ///<summary>R/W Voltage Channel Mapping Configuration P 42</summary>
            ChannelMapU = 0x02,
            ///<summary>R/W Sag and Peak Detector Period Configuration P 44</summary>
            SagPeakDetCfg = 0x05,
            ///<summary>R/W Over Voltage Threshold P 44</summary>
            OVth = 0x06,
            ///<summary>R/W Zero-Crossing Configuration Configuration of ZX0/1/2 pins’ source P 45</summary>
            ZXConfig = 0x07, 
            ///<summary>R/W Voltage Sag Threshold P 45</summary>
            SagTh = 0x08,
            ///<summary>R/W Voltage Phase Losing Threshold Similar to Voltage Sag Threshold register P 45</summary>
            PhaseLossTh = 0x09,
            ///<summary>R/W Neutral Current (Calculated) Warning Threshold P 46</summary>
            InWarnTh = 0x0A,
            ///<summary>R/W Over Current Threshold P 46</summary>
            OIth = 0x0B,
            ///<summary>R/W Low Threshold for Frequency Detection P 46</summary>
            FreqLoTh = 0x0C,
            ///<summary>R/W High Threshold for Frequency Detection P 46</summary>
            FreqHiTh = 0x0D,
            ///<summary>R/W Partial Measurement Mode Power Control P 47</summary>
            PMPwrCtrl = 0x0E,
            ///<summary>R/W IRQ0 Merge Configuration Refer to 4.2.2 Reliability Enhancement Feature P 47</summary>
            IRQ0MergeCfg = 0x0F,
        }

        /// <summary>
        /// Low Power Mode Register
        /// </summary>
        public enum RegisterLowPower: Byte
        {
            ///<summary>R/W Current Detect Control P 48</summary>
            DetectCtrl = 0x10,
            ///<summary>R/W Channel 1 Current Threshold in Detection Mode P 49</summary>
            DetectTh1 = 0x11, 
            ///<summary>R/W Channel 2 Current Threshold in Detection Mode P 49</summary>
            DetectTh2 = 0x12,
            ///<summary>R/W Channel 3 Current Threshold in Detection Mode P 49</summary>
            DetectTh3 = 0x13,
            ///<summary>R/W Phase A Current DC offset P 50</summary>
            IDCoffsetA = 0x14,
            ///<summary>R/W Phase B Current DC offset P 50</summary>
            IDCoffsetB = 0x15,
            ///<summary>R/W Phase C Current DC offset P 50</summary>
            IDCoffsetC = 0x16,
            ///<summary>R/W Voltage DC offset for Channel A P 50</summary>
            UDCoffsetA = 0x17,
            ///<summary>R/W Voltage DC offset for Channel B P 50</summary>
            UDCoffsetB = 0x18,
            ///<summary>R/W Voltage DC offset for Channel C P 51</summary>
            UDCoffsetC = 0x19,
            ///<summary>R/W Voltage Gain Temperature Compensation for Phase A/B P 51</summary>
            UGainTAB = 0x1A,
            ///<summary>R/W Voltage Gain Temperature Compensation for Phase C P 51</summary>
            UGainTC = 0x1B,
            ///<summary>R/W Phase Compensation for Frequency P 51</summary>
            PhiFreqComp = 0x1C,
            ///<summary>R/W Current (Log Irms0) Configuration for Segment Compensation P 51</summary>
            LOGIrms0 = 0x20,
            ///<summary>R/W Current (Log Irms1) Configuration for Segment Compensation P 51</summary>
            LOGIrms1 = 0x21,
            ///<summary>R/W Nominal Frequency P 52</summary>
            F0 = 0x22,
            ///<summary>R/W Nominal Temperature P 52</summary>
            T0 = 0x23,
            ///<summary>R/W Phase A Phase Compensation for Current Segment 0 and 1 P 52</summary>
            PhiAIrms01 = 0x24,
            ///<summary>R/W Phase A Phase Compensation for Current Segment 2 P 52</summary>
            PhiAIrms2 = 0x25,
            ///<summary>R/W Phase A Gain Compensation for Current Segment 0 and 1 P 53</summary>
            GainAIrms01 = 0x26,
            ///<summary>R/W Phase A Gain Compensation for Current Segment 2 P 53</summary>
            GainAIrms2 = 0x27, 
            ///<summary>R/W Phase B Phase Compensation for Current Segment 0 and 1 P 53</summary>
            PhiBIrms01 = 0x28,
            ///<summary>R/W Phase B Phase Compensation for Current Segment 2 P 54</summary>
            PhiBIrms2 = 0x29, 
            ///<summary>R/W Phase B Gain Compensation for Current Segment 0 and 1 P 53</summary>
            GainBIrms01 = 0x2A,
            ///<summary>R/W Phase B Gain Compensation for Current Segment 2 P 54</summary>
            GainBIrms2 = 0x2B, 
            ///<summary>R/W Phase C Phase Compensation for Current Segment 0 and 1 P 54</summary>
            PhiCIrms01 = 0x2C,
            ///<summary>R/W Phase C Phase Compensation for Current Segment 2 P 54</summary>
            PhiCIrms2 = 0x2D, 
            ///<summary>R/W Phase C Gain Compensation for Current Segment 0 and 1 P 54</summary>
            GainCIrms01 = 0x2E,
            ///<summary>R/W Phase C Gain Compensation for Current Segment 2 P 54</summary>
            GainCIrms2 = 0x2F, 
        }

        /// <summary>
        /// Configuration Registers 
        /// </summary>
        public enum RegistersConfiguration: Byte
        {
            /// <summary>
            /// Refer to 6.4.1 Start Registers and Associated Checksum Operation Scheme  Default Value: 6886H
            /// </summary>
            CalStart    = 0x30,


            ///<summary>R/W High Word of PL_Constant Refer to Table-6.P 55</summary>
            PLconstH = 0x31, 
            ///<summary>R/W Low Word of PL_Constant P 56</summary>
            PLconstL = 0x32,
            ///<summary>R/W Metering Method Configuration P 56</summary>
            MMode0 = 0x33, 
            ///<summary>R/W PGA Gain Configuration P 57</summary>
            MMode1 = 0x34, 
            ///<summary>R/W Active Startup Power Threshold</summary>
            PStartTh = 0x35, 
            ///<summary>R/W Reactive Startup Power Threshold</summary>
            QStartTh = 0x36, 
            ///<summary>R/W Apparent Startup Power Threshold</summary>
            SStartTh = 0x37, 
            ///<summary>R/W Startup Power Threshold for Any Phase(Active Energy Accumulation)</summary>
            PPhaseTh = 0x38, 
            ///<summary>R/W Startup Power Threshold for Any Phase(ReActive Energy Accumulation)</summary>
            QPhaseTh = 0x39,
            ///<summary>R/W Startup Power Threshold for Any Phase(Apparent Energy Accumulation)</summary>
            SPhaseTh = 0x3A 
        }

        /// <summary>
        /// Calibration Registers
        /// </summary>
        public enum RegistersCalibration: Byte
        {
            ///<summary>R/W Phase A Active Power offset Refer to Table-7.P 57</summary>
            PoffsetA = 0x41, 
            ///<summary>R/W Phase A Reactive Power offset P 58</summary>
            QoffsetA = 0x42, 
            ///<summary>R/W Phase B Active Power offset</summary>
            PoffsetB = 0x43, 
            ///<summary>R/W Phase B Reactive Power offset</summary>
            QoffsetB = 0x44, 
            ///<summary>R/W Phase C Active Power offset</summary>
            PoffsetC = 0x45, 
            ///<summary>R/W Phase C Reactive Power offset</summary>
            QoffsetC = 0x46,
            ///<summary>R/W Phase A Calibration Gain P 58</summary>
            PQGainA = 0x47,
            ///<summary>R/W Phase A Calibration Phase Angle P 58</summary>
            PhiA = 0x48, 
            ///<summary>R/W Phase B Calibration Gain</summary>
            PQGainB = 0x49,
            ///<summary>R/W Phase B Calibration Phase Angle</summary>
            PhiB = 0x4A, 
            ///<summary>R/W Phase C Calibration Gain</summary>
            PQGainC = 0x4B,
            ///<summary>R/W Phase C Calibration Phase Angle</summary>
            PhiC = 0x4C 
        }

        /// <summary>
        ///  Fundamental/Harmonic Energy Calibration Registers 
        /// </summary>
        public enum RegistersEnergyCalibration: Byte
        {
            ///<summary>R / W Phase A Fundamental Active Power offset Refer to Table - 8.</summary>
            PoffsetAF = 0x51,
            ///<summary>R / W Phase B Fundamental Active Power offset</summary>
            PoffsetBF = 0x52, 
            ///<summary>R / W Phase C Fundamental Active Power offset</summary>
            PoffsetCF = 0x53,
            ///<summary>R / W Phase A Fundamental Calibration Gain</summary>
            PGainAF = 0x54, 
            ///<summary>R / W Phase B Fundamental Calibration Gain</summary>
            PGainBF = 0x55, 
            ///<summary>R / W Phase C Fundamental Calibration Gain</summary>
            PGainCF = 0x56, 
        }

        /// <summary>
        /// Measurement Calibration Registers 
        /// </summary>
        public enum RegistersMeasurementCalibration: Byte
        {
            ///<summary>R/W Phase A Voltage RMS Gain Refer to Table-9.</summary>
            UgainA = 0x61, 
            ///<summary>R/W Phase A Current RMS Gain</summary>
            IgainA = 0x62, 
            ///<summary>R/W Phase A Voltage RMS offset</summary>
            UoffsetA = 0x63, 
            ///<summary>R/W Phase A Current RMS offset</summary>
            IoffsetA = 0x64,
            ///<summary>R/W Phase B Voltage RMS Gain</summary>
            UgainB = 0x65, 
            ///<summary>R/W Phase B Current RMS Gain</summary>
            IgainB = 0x66, 
            ///<summary>R/W Phase B Voltage RMS offset</summary>
            UoffsetB = 0x67, 
            ///<summary>R/W Phase B Current RMS offset</summary>
            IoffsetB = 0x68,
            ///<summary>R/W Phase C Voltage RMS Gain</summary>
            UgainC = 0x69, 
            ///<summary>R/W Phase C Current RMS Gain</summary>
            IgainC = 0x6A, 
            ///<summary>R/W Phase C Voltage RMS offset</summary>
            UoffsetC = 0x6B, 
            ///<summary>R/W Phase C Current RMS offset</summary>
            IoffsetC = 0x6C, 
        }



        /// <summary>
        /// EMM Status Registers
        /// </summary>
        public enum RegisterEMMStatus : Byte
        {
            ///<summary>R/W Software Reset P 59</summary>
            SoftReset = 0x70,
            ///<summary>R EMM State 0 P 60</summary>
            EMMState0 = 0x71,
            ///<summary>R EMM State 1 P 61</summary>
            EMMState1 = 0x72,
            ///<summary>R/W1C EMM Interrupt Status 0 P 62</summary>
            EMMIntState0 = 0x73,
            ///<summary>R/W1C EMM Interrupt Status 1 P 63</summary>
            EMMIntState1 = 0x74,
            ///<summary>R/W EMM Interrupt Enable 0 P 64</summary>
            EMMIntEn0 = 0x75,
            ///<summary>R/W EMM Interrupt Enable 1 P 65</summary>
            EMMIntEn1 = 0x76,
            ///<summary>R Last Read/Write SPI Value P 65</summary>
            LastSPIData = 0x78,
            ///<summary>R CRC Error Status P 66</summary>
            CRCErrStatus = 0x79,
            ///<summary>R/W CRC Digest P 66</summary>
            CRCDigest = 0x7A,
            ///<summary>R/W Configure Register Access Enable P 66 Energy Register</summary>
            CfgRegAccEn = 0x7F,
            ///<summary>R/C Total Forward Active Energy Refer to Table-11.P 67</summary>
        }
        
        /// <summary>
        /// Regular Energy Registers
        /// </summary>
        public enum RegisterEnergyRegisters : Byte
        {
            APenergyT = 0x80,
            ///<summary>R/C Phase A Forward Active Energy</summary>
            APenergyA = 0x81,
            ///<summary>R/C Phase B Forward Active Energy</summary>
            APenergyB = 0x82,
            ///<summary>R/C Phase C Forward Active Energy</summary>
            APenergyC = 0x83,
            ///<summary>R/C Total Reverse Active Energy</summary>
            ANenergyT = 0x84,
            ///<summary>R/C Phase A Reverse Active Energy</summary>
            ANenergyA = 0x85,
            ///<summary>R/C Phase B Reverse Active Energy</summary>
            ANenergyB = 0x86,
            ///<summary>R/C Phase C Reverse Active Energy</summary>
            ANenergyC = 0x87,
            ///<summary>R/C Total Forward Reactive Energy</summary>
            RPenergyT = 0x88,
            ///<summary>R/C Phase A Forward Reactive Energy</summary>
            RPenergyA = 0x89,
            ///<summary>R/C Phase B Forward Reactive Energy</summary>
            RPenergyB = 0x8A,
            ///<summary>R/C Phase C Forward Reactive Energy</summary>
            RPenergyC = 0x8B,
            ///<summary>R/C Total Reverse Reactive Energy</summary>
            RNenergyT = 0x8C,
            ///<summary>R/C Phase A Reverse Reactive Energy</summary>
            RNenergyA = 0x8D,
            ///<summary>R/C Phase B Reverse Reactive Energy</summary>
            RNenergyB = 0x8E,
            ///<summary>R/C Phase C Reverse Reactive Energy</summary>
            RNenergyC = 0x8F,
            ///<summary>R/C Total(Arithmetic Sum) Apparent Energy</summary>
            SAenergyT = 0x90,
            ///<summary>R/C Phase A Apparent Energy</summary>
            SenergyA = 0x91,
            ///<summary>R/C Phase B Apparent Energy</summary>
            SenergyB = 0x92,
            ///<summary>R/C Phase C Apparent Energy</summary>
            SenergyC = 0x93,
        }



        /// <summary>
        /// Fundamental / Harmonic Energy Register 
        /// </summary>
        public enum RegisterEnergy : Byte
        {
            ///<summary>R/C Total Forward Active Fundamental Energy Refer to Table-12.P 68</summary>
            APenergyTF = 0xA0,
            ///<summary>R/C Phase A Forward Active Fundamental Energy</summary>
            APenergyAF = 0xA1, 
            ///<summary>R/C Phase B Forward Active Fundamental Energy</summary>
            APenergyBF = 0xA2, 
            ///<summary>R/C Phase C Forward Active Fundamental Energy</summary>
            APenergyCF = 0xA3, 
            ///<summary>R/C Total Reverse Active Fundamental Energy</summary>
            ANenergyTF = 0xA4, 
            ///<summary>R/C Phase A Reverse Active Fundamental Energy</summary>
            ANenergyAF = 0xA5, 
            ///<summary>R/C Phase B Reverse Active Fundamental Energy</summary>
            ANenergyBF = 0xA6, 
            ///<summary>R/C Phase C Reverse Active Fundamental Energy</summary>
            ANenergyCF = 0xA7, 
            ///<summary>R/C Total Forward Active Harmonic Energy</summary>
            APenergyTH = 0xA8, 
            ///<summary>R/C Phase A Forward Active Harmonic Energy</summary>
            APenergyAH = 0xA9, 
            ///<summary>R/C Phase B Forward Active Harmonic Energy</summary>
            APenergyBH = 0xAA, 
            ///<summary>R/C Phase C Forward Active Harmonic Energy</summary>
            APenergyCH = 0xAB, 
            ///<summary>R/C Total Reverse Active Harmonic Energy</summary>
            ANenergyTH = 0xAC, 
            ///<summary>R/C Phase A Reverse Active Harmonic Energy</summary>
            ANenergyAH = 0xAD, 
            ///<summary>R/C Phase B Reverse Active Harmonic Energy</summary>
            ANenergyBH = 0xAE, 
            ///<summary>R/C Phase C Reverse Active Harmonic Energy</summary>
            ANenergyCH = 0xAF,
        }

        /// <summary>
        /// Power and Power Factor Registers 
        /// </summary>
        public enum RegistersPowerFactor : Byte
        {
            ///<summary>R Total(all -phase - sum) Active Power Refer to Table - 13.P 69</summary>
            PmeanT = 0xB0, 
            ///<summary>R Phase A Active Power</summary>
            PmeanA = 0xB1, 
            ///<summary>R Phase B Active Power</summary>
            PmeanB = 0xB2, 
            ///<summary>R Phase C Active Power</summary>
            PmeanC = 0xB3, 
            ///<summary>R Total(all - phase - sum) Reactive Power</summary>
            QmeanT = 0xB4, 
            ///<summary>R Phase A Reactive Power</summary>
            QmeanA = 0xB5, 
            ///<summary>R Phase B Reactive Power</summary>
            QmeanB = 0xB6, 
            ///<summary>R Phase C Reactive Power</summary>
            QmeanC = 0xB7, 
            ///<summary>R Total(Arithmetic Sum) Apparent Power</summary>
            SmeanT = 0xB8, 
            ///<summary>R Phase A Apparent Power</summary>
            SmeanA = 0xB9, 
            ///<summary>R Phase B Apparent Power</summary>
            SmeanB = 0xBA, 
            ///<summary>R Phase C Apparent Power</summary>
            SmeanC = 0xBB, 
            ///<summary>R Total Power Factor</summary>
            PFmeanT = 0xBC, 
            ///<summary>R Phase A Power Factor</summary>
            PFmeanA = 0xBD, 
            ///<summary>R Phase B Power Factor</summary>
            PFmeanB = 0xBE, 
            ///<summary>R Phase C Power Factor</summary>
            PFmeanC = 0xBF, 
            ///<summary>R Lower Word of Total(all - phase - sum) Active Power</summary>
            PmeanTLSB = 0xC0, 
            ///<summary>R Lower Word of Phase A Active Power</summary>
            PmeanALSB = 0xC1, 
            ///<summary>R Lower Word of Phase B Active Power</summary>
            PmeanBLSB = 0xC2, 
            ///<summary>R Lower Word of Phase C Active Power</summary>
            PmeanCLSB = 0xC3, 
            ///<summary>R Lower Word of Total(all - phase - sum) Reactive Power</summary>
            QmeanTLSB = 0xC4, 
            ///<summary>R Lower Word of Phase A Reactive Power</summary>
            QmeanALSB = 0xC5, 
            ///<summary>R Lower Word of Phase B Reactive Power</summary>
            QmeanBLSB = 0xC6, 
            ///<summary>R Lower Word of Phase C Reactive Power</summary>
            QmeanCLSB = 0xC7, 
            ///<summary>R Lower Word of Total(Arithmetic Sum) Apparent Power</summary>
            SAmeanTLSB = 0xC8,
            ///<summary>R Lower Word of Phase A Apparent Power</summary>
            SmeanALSB = 0xC9, 
            ///<summary>R Lower Word of Phase B Apparent Power</summary>
            SmeanBLSB = 0xCA, 
            ///<summary>R Lower Word of Phase C Apparent Power</summary>
            SmeanCLSB = 0xCB, 
        }

        /// <summary>
        /// Fundamental / Harmonic Power and Voltage / Current RMS Registers 
        /// </summary>
        public enum RegistersPower: Byte
        {
            ///<summary>R Total Active Fundamental Power Refer to Table - 14.P 70</summary>
            PmeanTF = 0xD0, 
            ///<summary>R Phase A Active Fundamental Power</summary>
            PmeanAF = 0xD1, 
            ///<summary>R Phase B Active Fundamental Power</summary>
            PmeanBF = 0xD2, 
            ///<summary>R Phase C Active Fundamental Power</summary>
            PmeanCF = 0xD3, 
            ///<summary>R Total Active Harmonic Power</summary>
            PmeanTH = 0xD4, 
            ///<summary>R Phase A Active Harmonic Power</summary>
            PmeanAH = 0xD5, 
            ///<summary>R Phase B Active Harmonic Power</summary>
            PmeanBH = 0xD6, 
            ///<summary>R Phase C Active Harmonic Power</summary>
            PmeanCH = 0xD7,
            ///<summary>R Phase A Voltage RMS</summary>
            UrmsA = 0xD9, 
            ///<summary>R Phase B Voltage RMS</summary>
            UrmsB = 0xDA, 
            ///<summary>R Phase C Voltage RMS</summary>
            UrmsC = 0xDB, 
            ///<summary>R N Line Calculated Current RMS</summary>
            IrmsN = 0xDC, 
            ///<summary>R Phase A Current RMS</summary>
            IrmsA = 0xDD, 
            ///<summary>R Phase B Current RMS</summary>
            IrmsB = 0xDE, 
            ///<summary>R Phase C Current RMS</summary>
            IrmsC = 0xDF, 
            ///<summary>R Lower Word of Total Active Fundamental Power</summary>
            PmeanTFLSB = 0xE0, 
            ///<summary>R Lower Word of Phase A Active Fundamental Power</summary>
            PmeanAFLSB = 0xE1, 
            ///<summary>R Lower Word of Phase B Active Fundamental Power</summary>
            PmeanBFLSB = 0xE2, 
            ///<summary>R Lower Word of Phase C Active Fundamental Power</summary>
            PmeanCFLSB = 0xE3, 
            ///<summary>R Lower Word of Total Active Harmonic Power</summary>
            PmeanTHLSB = 0xE4, 
            ///<summary>R Lower Word of Phase A Active Harmonic Power</summary>
            PmeanAHLSB = 0xE5, 
            ///<summary>R Lower Word of Phase B Active Harmonic Power</summary>
            PmeanBHLSB = 0xE6, 
            ///<summary>R Lower Word of Phase C Active Harmonic Power</summary>
            PmeanCHLSB = 0xE7,
            ///<summary>R Lower Word of Phase A Voltage RMS</summary>
            UrmsALSB = 0xE9, 
            ///<summary>R Lower Word of Phase B Voltage RMS</summary>
            UrmsBLSB = 0xEA, 
            ///<summary>R Lower Word of Phase C Voltage RMS</summary>
            UrmsCLSB = 0xEB, 
            ///<summary>R Lower Word of Phase A Current RMS</summary>
            IrmsALSB = 0xED, 
            ///<summary>R Lower Word of Phase B Current RMS</summary>
            IrmsBLSB = 0xEE, 
            ///<summary>R Lower Word of Phase C Current RMS</summary>
            IrmsCLSB = 0xEF, 
        }

        /// <summary>
        /// Peak, Frequency, Angle and Temperature Registers 
        /// </summary>
        public enum RegistersPeak: Byte
        {
            ///<summary>R Channel A Voltage Peak Refer to Table - 15.P 71</summary>
            UPeakA = 0xF1, 
            ///<summary>R Channel B Voltage Peak P 71</summary>
            UPeakB = 0xF2, 
            ///<summary>R Channel C Voltage Peak</summary>
            UPeakC = 0xF3, 
            ///<summary>R Channel A Current Peak</summary>
            IPeakA = 0xF5, 
            ///<summary>R Channel B Current Peak</summary>
            IPeakB = 0xF6, 
            ///<summary>R Channel C Current Peak</summary>
            IPeakC = 0xF7,
            ///<summary>R Frequency</summary>
            Freq = 0xF8, 
            ///<summary>R Phase A Mean Phase Angle</summary>
            PAngleA = 0xF9, 
            ///<summary>R Phase B Mean Phase Angle</summary>
            PAngleB = 0xFA, 
            ///<summary>R Phase C Mean Phase Angle</summary>
            PAngleC = 0xFB,
            ///<summary>R Measured Temperature</summary>
            Temp = 0xFC, 
            ///<summary>R Phase A Voltage Phase Angle</summary>
            UangleA = 0xFD, 
            ///<summary>R Phase B Voltage Phase Angle</summary>
            UangleB = 0xFE, 
            ///<summary>R Phase C Voltage Phase Angle</summary>
            UangleC = 0xFF, 
        }
    }
}