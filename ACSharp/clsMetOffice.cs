using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Serilog;


namespace ACSharp;

public class clsMetOffice
{

    static double latitude = 50.856461; // Replace with your latitude
    static double longitude = -1.280049; // Replace with your longitude
    static string clientID {get; set;}
    static string clientKey {get; set;}

    static DateTime LastUpdated = DateTime.MinValue;

    private static string apiUrl = null;

    public static List<clsMetOfficeData.TimeSeries> TimeSeriesList = null;


    static clsMetOffice()
    {

        clientID = Environment.GetEnvironmentVariable("METOFFICE_ID", EnvironmentVariableTarget.Process) ?? "";
        clientKey = Environment.GetEnvironmentVariable("METOFFICE_SECRET", EnvironmentVariableTarget.Process) ?? "";

        apiUrl = $"https://api-metoffice.apiconnect.ibmcloud.com/v0/forecasts/point/hourly?latitude={latitude}&longitude={longitude}&excludeParameterMetadata=true&includeLocationName=false";
    }


    public static async Task GetTimeSeries(DateTime StartDay)
    {

        if (LastUpdated > DateTime.UtcNow.AddMinutes(-30))
        {
            Log.Information("GetTimeSeries: Pulling from cache");
            return;
        }

        if (String.IsNullOrEmpty(clientID) || String.IsNullOrEmpty(clientKey))
        {
            Log.Error("clsMetOffice: Missing clientID or clientKey");
            return;
        }


        using (HttpClient client = new HttpClient())
        {
            client.DefaultRequestHeaders.Add("X-IBM-Client-Id", clientID);
            client.DefaultRequestHeaders.Add("X-IBM-Client-Secret", clientKey);
            client.DefaultRequestHeaders.Add("accept", "application/json");

            HttpResponseMessage response = await client.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {

                string jsonResponse = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<clsMetOfficeData>(jsonResponse);

                DateTime StartTime = new DateTime(StartDay.Year, StartDay.Month, StartDay.Day, 0, 0, 0);
                DateTime EndTime = StartTime.AddDays(1).AddSeconds(-1);

                TimeSeriesList = data.features.First().properties.timeSeries.Where(x=>x.time >= StartTime && x.time <= EndTime).ToList();

                LastUpdated = DateTime.UtcNow;
                Log.Information($"MetOffice Data: {StartTime}->{EndTime}");
            }
            else
            {
                Log.Information("Failed to fetch solar irradiance data.");
            }
        }
    }

    public static async Task<uint> GetUVIndexCount(DateTime StartDay)
    {
        GetTimeSeries(StartDay).Wait();

        uint uvCount = 0;

        if (TimeSeriesList!=null && TimeSeriesList.Count>0)
        {

            uvCount = (uint)TimeSeriesList.Count(x=>x.uvIndex >=3);

            //uvCount = (uint)TimeSeriesList.Sum(x=>x.uvIndex);
            Log.Information($"{TimeSeriesList.Min(x=>x.time)}->{TimeSeriesList.Max(x=>x.time)} uVCount = {uvCount}");
        }
        else
        {
            Log.Information("Failed to fetch solar irradiance data.");
        }

        return uvCount;
    }

    public static async Task<uint> GetUVIndexNow()
    {
        if (TimeSeriesList!=null && TimeSeriesList.Count>0)
        {
            var hourNow = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Hour, 0, 0, DateTimeKind.Utc);
            return TimeSeriesList.Find(x => x.time >= hourNow && x.time < hourNow.AddHours(1))?.uvIndex ?? 0;
        }
        else
        {
            Log.Information("Failed to fetch solar irradiance data.");
        }

        return 0;
    }
}