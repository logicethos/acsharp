﻿using System;
using System.Linq;
using System.Reflection;

namespace ACSharp;


[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AssemblyGitCommit : Attribute
{
    public string Hash;

    public AssemblyGitCommit() : this(string.Empty) { }
    public AssemblyGitCommit(string hash) { Hash = hash; }

    public static string GetValue()
    {
        var GitCommit = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyGitCommit>();
        if (GitCommit != null && GitCommit.Any()) return GitCommit.First().Hash; else return null;
    }
}

[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AssemblyGitCommitDateTime : Attribute
{
    public String? CommitTime;

    public AssemblyGitCommitDateTime() : this(string.Empty) { }
    public AssemblyGitCommitDateTime(string unixTime) { CommitTime = unixTime; }

    public static DateTime? GetValue()
    {
        var GitCommit = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyGitCommitDateTime>();
        if (GitCommit == null || !GitCommit.Any()) return null;

        long unixtime;
        if (long.TryParse(GitCommit.First().CommitTime,out unixtime))
        {
            return DateTime.UnixEpoch.AddSeconds(unixtime);
        }

        return null;
    }
}

[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AssemblyGitCommitTag : Attribute
{
    public string Tag;

    public AssemblyGitCommitTag() : this(string.Empty) { }
    public AssemblyGitCommitTag(string tag) { Tag = tag; }

    public static string? GetValue()
    {
        var GitCommit = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyGitCommitTag>();
        if (GitCommit != null && GitCommit.Any())
        {
            var tag = GitCommit.First().Tag;
            if (!String.IsNullOrEmpty(tag) && !tag.Contains('-')) return $"{tag}-0";
            return tag;
        }

        return null;
    }

    public static string? GetValueOrTime()
    {

        var tag = GetValue();
        if (String.IsNullOrEmpty(tag)) tag = $"{AssemblyGitCommitDateTime.GetValue():yyyy-MM-dd HH:mm}";
        return tag;
    }
}

[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AssemblyGitCommitBranch : Attribute
{
    public string Branch;

    public AssemblyGitCommitBranch() : this(string.Empty) { }
    public AssemblyGitCommitBranch(string branch) { Branch = branch; }

    public static string? GetValue()
    {
        var GitCommit = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyGitCommitBranch>();
        if (GitCommit != null && GitCommit.Any()) return GitCommit.First().Branch; else return null;
    }

    public override String? ToString()
    {
        return GetValue();
    }

}