using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ACSharp.GRPC;
using Google.Protobuf;
using Google.Protobuf.Reflection;
using LettuceEncrypt;
using LettuceEncrypt.Acme;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Events;
using AuthenticationService = ACSharp.GRPC.AuthenticationService;


namespace ACSharp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey =
                        new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtAuthenticationManager.JWT_TOKEN_KEY)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                };
            });
            services.AddAuthorization();
            services.AddGrpc();
            /*services.AddRazorPages()
                .AddRazorPagesOptions(options =>
                {
                    //Use the below line to change the default directory
                    //for your Razor Pages.
                    options.RootDirectory = Path.Combine("/",Program.DataPath, "www");
                
                    //Use the below line to change the default
                    //"landing page" of the application.
                    //options.Conventions.AddPageRoute(Path.Combine(Program.DataPath, "www","index.razor") , "");
              
                  
                });*/

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                Program.CORSOptions = o;
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                //  .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
            }));
            
            services.AddCors(o => o.AddPolicy( Program.CORSpolicy, builder =>
            {
                var corsList = new List<string>();
                foreach (var domain in Program.Settings.DomainNames)
                {
                    corsList.Add($"https://{domain}");
                }
                corsList.AddRange(Program.Settings.CorsURL);
                
                Log.Information("CORS: {@list}",corsList);

                builder.WithOrigins(corsList.Distinct().ToArray())
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowedToAllowWildcardSubdomains();
                //  .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
            }));
#if !DEBUG
            if (Program.Settings.DomainNames?.Count > 0 && Program.SSLCert == null)
            {
                if (String.IsNullOrEmpty(Program.Settings.DomainEmail))
                {
                    Log.Error("Domain Name e-mail missing.  Skipping Lets Encrypt.");
                }
                else
                {
                       services.AddLettuceEncrypt(c =>
                        {
                            /*c.EabCredentials = new EabCredentials
                            {
                                EabKeyId = null,
                                EabKey = null,
                                EabKeyAlg = null
                            }*/
                            c.DomainNames = Program.Settings.DomainNames.ToArray();
                            c.EmailAddress = Program.Settings.DomainEmail;
                            c.AcceptTermsOfService = true;
                            c.RenewDaysInAdvance = TimeSpan.FromDays(5);
                            c.AllowedChallengeTypes = ChallengeType.TlsAlpn01;

                            //c.UseStagingServer = true;

                        })
                        .PersistDataToDirectory(new DirectoryInfo(Program.DataPathCerts), null);
                }
            }
#endif
        //    services.AddControllers();
         //   services.AddRouting();
         services.AddMetrics();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSerilogRequestLogging(options =>
            {
                // Customize the message template
                options.MessageTemplate = "Handled {RequestPath}";
    
                // Emit debug-level events instead of the defaults
                options.GetLevel = (httpContext, elapsed, ex) => LogEventLevel.Debug;
    
                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    diagnosticContext.Set("RequestHost", httpContext.Request.Host.Value);
                    diagnosticContext.Set("RequestScheme", httpContext.Request.Scheme);
                };
            });

            app.UseRouting();
            app.UseGrpcWeb(); 
            app.UseCors();

            if (!String.IsNullOrEmpty(Program.DataPathWWW) &&  Directory.Exists(Program.DataPathWWW))
            {
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Program.DataPathWWW),
                    RequestPath = ""
                });
            }

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
             //   endpoints.MapRazorPages();
             //   endpoints.MapControllers();
                endpoints.MapGrpcService<AuthenticationService>().EnableGrpcWeb().RequireCors("AllowAll");
                endpoints.MapGrpcService<ApiService>().EnableGrpcWeb().RequireCors("AllowAll");
               // endpoints.MapFallbackToPage("/Index.razor");

                /*endpoints.MapGet("/",
                    async context =>
                    {
                        await context.Response.WriteAsync(
                            "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                    });*/

                endpoints.MapGet("/rpc",
                    async context =>
                    {
                        /*using (var sr = new System.IO.StreamReader(context.Request.Body))
                        {
                            string content = await sr.ReadToEndAsync();
                            var ipRequest = JsonSerializer.Deserialize<REST.GetIP>(content);

                            var nodes = Program.DistributedProxyList.GetRandomAccumulateNodes(ipRequest.@params.network,ipRequest.@params.count);
                            
                            var ipReply = new GetIPReply
                            {
                                jsonrpc = ipRequest.jsonrpc,
                                result = new GetIPReply.Result
                                {
                                    network = null,
                                    addresses = default
                                },
                                id = ipRequest.id
                            };

                            await context.Response.WriteAsync(JsonSerializer.Serialize<REST.GetIPReply>(ipReply));
                        }*/
                    });
            });
            
        }
    }
}