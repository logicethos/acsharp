using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using ACSharp.GRPC;
using ACSharp.TaskManager;
using ACSharp.TelnetConsole;
using ACSharpCommon.Proto;
using ACSharpDriver_RaspberryPi;
using ConsoleServer;
using ConsoleServerDocker;
using Google.Protobuf;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Mono.Unix;
using Mono.Unix.Native;
using Proto.Settings;
using Serilog;
using Serilog.Events;
using SpiDriver;

namespace ACSharp
{
    public class Program
    {

        static public Proto.Settings.Settings Settings { get; private set; }

        static public string DataPath { get; private set; } = "data";
        static public string? DataPathWWW { get; private set; }
        static public string? DataPathCerts { get; set; } = "/certs";
        static public CancellationTokenSource CancelTokenSource = new CancellationTokenSource();
        static public string VersionTag { get; private set; }
        public static M90E32AS em;
        static public clsDockerManager DockerManager;
        static public X509Certificate2? SSLCert = null;

        static public CorsOptions CORSOptions;
        static public readonly string CORSpolicy = "CorsPolicy";
        static public InfluxClient Influx;

       // static public Gauge WeatherGuageUVCount { get; set; }
       // static public Gauge WeatherGuageUV { get; set; }

        static public ACSharpDriver_RaspberryPi.IO2 PiIO { get; set; }

        static public bool RestartRequired { get; set; }
        static public bool NewVersion { get; set; }

        public static bool DisableHotWaterTimer { get; set; } = false;
        private static bool _hotWaterTimer;
        static public bool HotWaterTimer
        {
            get
            {
                return _hotWaterTimer;
            }
            set
            {

                if (!DisableHotWaterTimer)
                {

                    if (value)
                    {
   //                     if (Program.WeatherGuageUVCount.Value<1) //No sun expected
                        {
                            if (Program.PiIO.H0Temp.Celsius < 50) Program.PiIO.SetPercentage(100, 0);
                            else if (Program.PiIO.H0Temp.Celsius < 63) Program.PiIO.SetPercentage(25, 0);
                            else if (Program.PiIO.H1Temp.Celsius < 40) Program.PiIO.SetPercentage(0, 25);
                            else Program.PiIO.SetPercentage(0, 0);
                        }
                        /*else
                        {
                            if (Program.PiIO.H0Temp.Celsius < 50) Program.PiIO.SetPercentage(25, 0);
                            else if (Program.PiIO.H1Temp.Celsius < 30) Program.PiIO.SetPercentage(0, 25);
                            else Program.PiIO.SetPercentage(0, 0);
                        }*/
                    }
                    else if (Program.PiIO.PWM0DutyCycle > 0 || Program.PiIO.PWM1DutyCycle > 0)
                    {
                        Program.PiIO.SetPercentage(0, 0);
                    }
                }

                if (_hotWaterTimer != value)
                {
                    if (DisableHotWaterTimer || !value) Program.PiIO.SetPercentage(0, 0);
                    _hotWaterTimer = value;
                }
            }
        }


        static private int SolarModeCount;

        static private int _GridWatts;
        static public int GridWatts
        {
            get
            {
                return _GridWatts;
            }
            set
            {
                _GridWatts = value;

                /*
                if (( Program.PiIO.H1Temp?.Celsius <= 60 &&  ((_GridWatts < 30 && SolarModeCount == 0 )) || SolarModeCount > 2))
                {

                    var newWatts = Program.PiIO.Watts0 + Program.PiIO.Watts1 - _GridWatts - 100;  //100 buffer.

                    Log.Information($"solar: Grid {_GridWatts} of which HW={Program.PiIO.Watts0}/{Program.PiIO.Watts1} NewWatts: {newWatts}");

                    if (newWatts > 0)
                    {
                        if ( Program.PiIO.H0Temp?.Celsius <= 60)
                            Program.PiIO.SetWatts((uint)newWatts,0);
                        else
                            Program.PiIO.SetWatts(0, (uint)newWatts);

                        Log.Information($"solar: Setting desired watts: {newWatts}");
                        SolarModeCount = 1;
                    }
                    else
                    {
                        Log.Information($"solar: reset");
                        Program.PiIO.SetWatts(0, 0);
                        SolarModeCount = 0;
                    }

                }
                else if (SolarModeCount > 0)
                {
                    if (Program.PiIO?.H1Temp?.Celsius > 60)
                    {
                        Log.Information($"solar: max temp");
                        Program.PiIO.SetWatts(0, 0);
                        SolarModeCount = 0;
                    }
                    else
                    {
                        SolarModeCount++;
                        Log.Information($"solar: skip");
                    }
                }
            */
            }

        }

        public static void Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
#if DEBUG
                .WriteTo.ConsoleLocal(restrictedToMinimumLevel: LogEventLevel.Information)
#else
            .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
#endif
                //  .WriteTo.File("logfile.log", rollingInterval: RollingInterval.Day)
                .WriteTo.ConsoleTelnetServer()
                .CreateLogger();

            var env_influxDB = Environment.GetEnvironmentVariable("INFLUXDB")?.Split(",", StringSplitOptions.TrimEntries);
            if (env_influxDB == null || env_influxDB.Length != 3 || env_influxDB[2].Length < 10) {Console.WriteLine($"Incorrect INFLUXDB {env_influxDB[0]} {env_influxDB[1]}"); return;}


            var GitHash = AssemblyGitCommit.GetValue();
            if (!String.IsNullOrEmpty(GitHash))
            {
                VersionTag = AssemblyGitCommitTag.GetValue();
                Log.Information("Git Hash: {GitHash}",GitHash);
                Log.Information("Git Branch: {Branch}  Tag: {Tag} {date:yyyy-MM-dd HH:mm:ss}",AssemblyGitCommitBranch.GetValue(),VersionTag,AssemblyGitCommitDateTime.GetValue());
            }
            else
            {
                Log.Information("Git Hash: missing");
            }


            DataPath = Environment.GetEnvironmentVariable("ACSharp_DATA", EnvironmentVariableTarget.Process) ?? "/data";
            if (!Directory.Exists(DataPath))
            {
#if !DEBUG
            Log.Fatal("Data path not found: {path}",DataPath);
            return;
#else
                DataPath = "/home/stuart/Projects/ACSharp/ACSharp/Config";
                DataPathCerts = DataPath;
#endif
            }

            DataPathWWW = Environment.GetEnvironmentVariable("ACSharp_WWW", EnvironmentVariableTarget.Process) ?? Path.Combine(DataPath,"www");

            DockerManager = new clsDockerManager("acsharp","registry.gitlab.com/logicethos/acsharp",CancelTokenSource);

            Influx = new InfluxClient(env_influxDB[0], env_influxDB[1], env_influxDB[2]);

            var telnetServer = new ConsoleServer.TelnetServer(3333);  //(int)Settings.ConsolePort
            Log.Information("Starting Telnet Server");
            telnetServer.StartListen( new Action<TelnetSession>((session) =>
            {
                var telnetAppSession = new clsTelnetAppSession(session, Log.Logger);
                telnetAppSession.Run();
            }));

            Influx.Open();
          //em = new M90E32AS();
         // em.Connect("/dev/ttyUSB1");

         // var xxx1 = em.Read(atm90e26.Registers.CRC_1);
          
        // var freq = em.ReadUInt16((UInt16)M90E32AS.RegistersPeak.Freq);
       //  var temp = em.Read16((UInt16)M90E32AS.RegistersPeak.Temp);

         
        
         // var xxx = em.Read(0xD9);
          
         // var api = new ApiService(null);
          
         // var result0 = api.GetPower(null,null);
          
       //   var result1 = api.GetSettings(null,null);
       //   result1.Result.MeterEnable = true;
     //     var result2 = api.SetSettings(result1.Result,null);
          
     //     var result3 = api.GetPower(null, null);
          

        //  CreateHostBuilder(args).Build().Run();

          /*foreach (var regitem in atm90e26.m90e26DataReg)
          {
              var datain = new byte[2];
              var dataout = new byte[] {(byte) regitem};

              spi.SetOutput(output, true);
              spi.Write(dataout, 0, 1);
              spi.Read(datain, 0, 2);
              spi.SetOutput(output, false);
              
              Console.WriteLine($"{Enum.GetName(regitem)} {BitConverter.ToString(datain)}");
          }*/

          if (Settings == null)
          {
              Settings = new Proto.Settings.Settings();
          }


          Settings.WWWPort = 8080;

          Log.Information($"Start clsWebHostBuilder");
          var hostBuilder = new clsWebHostBuilder();
          var hostTask = hostBuilder.Build().RunAsync(CancelTokenSource.Token);

          PiIO = new IO2();
          clsTaskManager.Run();


          UnixSignal[] signals = new UnixSignal[] {
              new UnixSignal(Signum.SIGQUIT),
              new UnixSignal(Signum.SIGTERM),
          };

          do
          {
              int id = UnixSignal.WaitAny(signals,30000);
              if (id >= 0 && id < signals.Length)
              {
                  if (signals[id].IsSet)
                  {
                      Log.Fatal($"UNIX Signal {signals[id]}");
                      CancelTokenSource.Cancel();
                      break;
                  }
              }
              clsTaskManager.Run();
          } while (!CancelTokenSource.IsCancellationRequested);

          Influx.Dispose();
          clsTaskManager.RemoveAllTasks();
          DockerManager.Dispose();
          telnetServer.Dispose();
          Log.CloseAndFlush();
        }

    }
}