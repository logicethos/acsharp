using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ACSharp;

public  class clsOpenWeatherMap
{
    static string apiKey = "ab31a4f2ddaee8a07c47e26763fa6795";
    static double latitude = 50.856461; // Replace with your latitude
    static double longitude = -1.280049; // Replace with your longitude
    static string apiUrl = $"https://api.openweathermap.org/data/2.5/onecall?lat={latitude}&lon={longitude}&exclude=current,minutely,daily,alerts&appid={apiKey}";


        public static async Task GetWeather()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    var data = JsonSerializer.Deserialize<clsMetOfficeData>(jsonResponse);

                    /*foreach (var hourlyData in weatherData["hourly"])
                    {
                        DateTime timestamp = DateTimeOffset.FromUnixTimeSeconds(hourlyData.Value<long>("dt")).DateTime;
                        double solarIrradiance = hourlyData.Value<double>("uvi");

                        Console.WriteLine($"Timestamp: {timestamp}, Solar Irradiance: {solarIrradiance} W/m²");
                    }*/
                }
                else
                {
                    Console.WriteLine("Failed to fetch solar irradiance data.");
                }
            }
        }

}