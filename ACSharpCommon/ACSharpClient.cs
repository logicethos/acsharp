﻿using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ACSharpCommon.Proto;
using Grpc.Core;
using Grpc.Net.Client;

namespace ACSharpCommon;

public class ACSharpClient : IDisposable
{
    private GrpcChannel Channel;
    private String HostURL;
    private GrpcChannelOptions Options;
    public ACSharpAPI.ACSharpAPIClient API { get; private set; }
        
    public ACSharpClient(string host = "https://localhost:5001")
    {
        HostURL = host;
        Options = new GrpcChannelOptions();

    }

    public bool Connect()
    {
        Channel = GrpcChannel.ForAddress(HostURL,Options);
        API =  new ACSharpAPI.ACSharpAPIClient(Channel);
        return true;
    }

    public void Dispose()
    {
            
        Channel.ShutdownAsync().Wait();
        Channel.Dispose();
    }
        
}